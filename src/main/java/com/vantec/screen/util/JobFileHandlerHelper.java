package com.vantec.screen.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vantec.screen.entity.E2Part;
import com.vantec.screen.entity.E2Recon;
import com.vantec.screen.entity.FtpPickplan;
import com.vantec.screen.entity.FtpReceiptplan;

public final class JobFileHandlerHelper {

	private static Logger LOGGER = LoggerFactory.getLogger(JobFileHandlerHelper.class);
	private static String[] splitLine(String line) {
		// Split Line at |
		line = line.replaceAll("\"", "");
		line = line.concat("|End");
		String[] values = line.split("\\|");
		return values;
	}

	public static List<E2Part> readDailyPartFile(File file) throws IOException {

		// Sample Lines
		// 01010D1025|BOLT (DLF)|.0800|N|0|4|.0270|7318158898|3|Purchased|PRD000266
		// PNO|Locn Description |Item Cost|DDC Part Flag|DDC Offset|Count Freq|Weight|Commodity|Safety Time|Purch.Text|Supplier
		FileReader fileReader = new FileReader(file);
		BufferedReader lines = new BufferedReader(fileReader);
		String line = "";
		List<String> partNumbers = new ArrayList<String>();

		List<E2Part> partDTOs = new ArrayList<E2Part>();
		while ((line = lines.readLine()) != null) {

			String[] values = splitLine(line);
			E2Part e2part = new E2Part();
			e2part.setPartNumber(values[0]);
			e2part.setLocationDesc(values[1]);
			e2part.setItemCost(Double.valueOf(values[2]));
			e2part.setDdcPartFlag(values[3]);
			e2part.setDdcOffset(values[4]);
			e2part.setCountFreq(values[5]);
			e2part.setWeight(Double.valueOf(values[6]));
			e2part.setCommodity(values[7]);
			e2part.setSafetyTime(Integer.valueOf(values[8]));
			e2part.setPurchaseText(values[9]);
			e2part.setSupplier(values[10]);
			e2part.setDateCreated(new Date());
			//Don't add duplicate parts
			if(!partNumbers.contains(values[0])){
				partDTOs.add(e2part);
				partNumbers.add(values[0]);
			}else{
				LOGGER.info("Duplicate part ignored.."+values[0] + " ... for vendor "+values[10]);
			}
		}

		fileReader.close();
		lines.close();

		return partDTOs;

	}
	
	public static List<FtpReceiptplan> readReceiptPlan(File file) throws IOException {

		// Sample Lines
		//PP0205160|1|2036259270|13|20191113|PRD000295|UCHIMURA INT EUROPE SRL-UK|N
		//PP0210949|1|WG46|416|20200225|IND000802|LUBRICANTS UK LTD|N
		FileReader fileReader = new FileReader(file);
		BufferedReader lines = new BufferedReader(fileReader);
		String line = "";

		List<FtpReceiptplan> records = new ArrayList<FtpReceiptplan>();
		while ((line = lines.readLine()) != null) {

			String[] values = splitLine(line);
			FtpReceiptplan receiptPlans = new FtpReceiptplan();
			receiptPlans.setPR1ORNO(values[0]);
			receiptPlans.setPR1PONO(values[1]);
			receiptPlans.setPR1PNO(values[2]);
			receiptPlans.setPR1PCQ(Integer.valueOf(values[3]));
			receiptPlans.setPR1RDT(Integer.valueOf(values[4]));
			receiptPlans.setPR1PCD(values[5]);
			receiptPlans.setPR1PNM(values[6]);
			receiptPlans.setPR1PFL(values[7]);
			receiptPlans.setDateCreated(new Date());
			records.add(receiptPlans);
		}

		fileReader.close();
		lines.close();

		return records;

	}
	public static List<FtpPickplan> readFtpPickPlan(File file) throws IOException {

		// Sample Lines
		//AA0003863|Planned|01-11-21 08:00|5|- (Planned Issue)|0101061655|4|2|DUMM|000049112PC170LC-11E0 BASE SUB|04-11-21 13:01| ||
		//AC0020776|Planned|06-09-21 09:54|4|- (Planned Issue)|0101062050|32|216|TA|000048246PC210LC-11E0 BASE SUB|09-09-21 08:07| ||
		FileReader fileReader = new FileReader(file);
		BufferedReader lines = new BufferedReader(fileReader);
		String line = "";

		List<FtpPickplan> records = new ArrayList<FtpPickplan>();
		while ((line = lines.readLine()) != null) {

			String[] values = splitLine(line);
			FtpPickplan pickPlan = new FtpPickplan();
			System.out.println(values[0]);
			 pickPlan.setPP1ORNO( values[0]) ;
			 pickPlan.setPP1STS( values[1]) ;
			 pickPlan.setPP1PSTMP( values[2]) ;
			 pickPlan.setPP1PONO(values[3]) ;
			 pickPlan.setPP1TYP(values[4]) ;
			if (pickPlan.getPP1TYP() != null && pickPlan.getPP1TYP().length() >= 14
					&& pickPlan.getPP1TYP().substring(11, 14).equalsIgnoreCase("ISS")) {
				 pickPlan.setPP1TTYPE("ISSUE");
			 }else{
				 pickPlan.setPP1TTYPE("RECEIPT");
			 }
			 pickPlan.setPP1PNO( values[5]) ;
			 pickPlan.setPP1PCQ(Integer.valueOf(values[6]));
			 pickPlan.setPP1OPER( values[7]);
			 pickPlan.setPP1ILOC( values[8]);
			 pickPlan.setPP1ITM( values[9]) ;
			 if (pickPlan.getPP1ITM()!=null && pickPlan.getPP1ITM().length() >=25 ){
				 pickPlan.setPP1PARENT(pickPlan.getPP1ITM().substring(9, 25));
    		     pickPlan.setPP1PROJECT(pickPlan.getPP1ITM().substring(0,9));
			 }
			 pickPlan.setPP1BSTMP( values[10]);
			 pickPlan.setPP1SSTMP(values[11]);
			 pickPlan.setPP1DSTR(values[12]) ;
			 pickPlan.setPP1DNM(values[13]) ;
			 if(!"End".equalsIgnoreCase(values[14])){
				 pickPlan.setPP1PROJ(values[14]);
			 }
			 pickPlan.setDateCreated(new Date());

			records.add(pickPlan);
		}

		fileReader.close();
		lines.close();

		return records;

	}
	
	
	public static List<E2Recon> readReconFile(File file) throws IOException {

		// Sample Lines
		//0101061655|N|8|0
		//0101062050|N|120|0
		FileReader fileReader = new FileReader(file);
		BufferedReader lines = new BufferedReader(fileReader);
		String line = "";

		List<E2Recon> records = new ArrayList<E2Recon>();
		while ((line = lines.readLine()) != null) {

			String[] values = splitLine(line);
			E2Recon e2Recon = new E2Recon();
			e2Recon.setPartNumber( values[0]) ;
			e2Recon.setBackFlush(values[1].equalsIgnoreCase("Y") ? true : false) ;
			e2Recon.setVimsQty(0.0);
			e2Recon.setKukQty(Double.valueOf(values[2]));
			e2Recon.setDateCreated(new Date());
			records.add(e2Recon);
		}
		fileReader.close();
		lines.close();

		return records;

	}
	

}
