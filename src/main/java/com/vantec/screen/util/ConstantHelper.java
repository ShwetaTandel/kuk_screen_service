package com.vantec.screen.util;

public interface ConstantHelper {

	
	public static final String VENDOR_KUK_REF_CODE = "KUK";
	
	public static final String LOCATION_NEW = "NEW";
	/**Document status table constants**/
	public static final String DOCUMENT_STATUS_COMPLETED = "COMPLETE";
	public static final String DOCUMENT_STATUS_TRANSMITTED = "TRANSMITTED";
	public static final String DOCUMENT_STATUS_QUARANTINE = "QUARANTINE";
	public static final String DOCUMENT_STATUS_OPEN = "OPEN";
	public static final String DOCUMENT_STATUS_DELETED = "DELETED"; 
	public static final String DOCUMENT_STATUS_CLOSED =  "CLOSED";
	public static final String RETURN_FILE_TYPE_UNPLANNED_RTS = "STOCKA";

	/**Inventory status table constants**/
	public static final String INVENTORY_STATUS_OK = "OK";
	public static final String INVENTORY_STATUS_QA = "QTINE";
	public static final String LOCATION_TYPE_QA = "QA";
	
	public static final String TRANSACTION_CODE_STOCK_MOVE_FS = "STKMVFS";
	
	public static final String FQA_REFERENCE_PREFIX = "FQA";
	
	
	
	/**General  constants**/
	public static final String SYSUSER = "SYS";
	public static final String DAILYJOBUSER = "Daily Job";
	
	public static final String RVS_FILE_JOB_USER = "RVS File Job";
	public static final String TMP_FILE_EXT = ".tmp";
	public static final String TEXT_FILE_EXT = ".txt";
	
	public static final String BAAN_FILE_START_FILE_RECORD = "Start of file";
	public static final String BAAN_FILE_END_FILE_RECORD = "End of file";
	
	public static final String UNPLANNED_RTS_FILE_RETURN_ORIGIN = "WA";
	public static final String UNPLANNED_RTS_FILE_RETURN_TYPE_GENERAL= "UNRTS";
	public static final String UNPLANNED_RTS_FILE_RETURN_TYPE_FOR_REASON_CODE_18= "RTSCC";
	public static final String UNPLANNED_REASON_CODE_RTS_PROJ= "RTSPROJECT";
	
	

}
