package com.vantec.screen.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.vantec.screen.entity.LocationAisle;
import com.vantec.screen.entity.LocationColumn;
import com.vantec.screen.entity.LocationRack;
import com.vantec.screen.entity.LocationRow;
import com.vantec.screen.entity.LocationSubType;
import com.vantec.screen.entity.LocationType;
import com.vantec.screen.entity.Part;
import com.vantec.screen.entity.Vendor;
import com.vantec.screen.model.AisleDTO;
import com.vantec.screen.model.ColumnDTO;
import com.vantec.screen.model.LocationTypeDTO;
import com.vantec.screen.model.PartDTO;
import com.vantec.screen.model.RackDTO;
import com.vantec.screen.model.RowDTO;
import com.vantec.screen.model.VendorDTO;
import com.vantec.screen.model.ZoneTypeDTO;

public final class ScreenServiceHelper {
	
	public static Vendor convertVendorViewToEntity(VendorDTO vendorDTO,Vendor vendor) throws ParseException {
		
		if(vendor == null){
			vendor = new Vendor();
			vendor.setDateCreated(new Date());
			vendor.setCreatedBy(vendorDTO.getCreatedBy());
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		vendor.setAutoPick(vendorDTO.getAutoPick());
		vendor.setAutoReceive(vendorDTO.getAutoReceive());
		vendor.setContactEmail(vendorDTO.getContactEmail());
		vendor.setContactName(vendorDTO.getContactName());
		vendor.setContactTel(vendorDTO.getContactTel());
		vendor.setCountryName(vendorDTO.getCountry());
		vendor.setConvertToLt(vendorDTO.getConvertToLt());
		vendor.setDateUpdated(new Date());
		vendor.setDecant(vendorDTO.getDecant());
		//vendor.setDecantChargeId(vendorDTO.getDecantCharge());
		//vendor.setDespatchChargeId(vendorDTO.getDespatchCharge());
		Date effectiveFromDate = sdf.parse(vendorDTO.getEffectiveFrom());
		vendor.setEffectiveFrom(effectiveFromDate);
		Date effectiveToDate = sdf.parse(vendorDTO.getEffectiveTo());
		vendor.setEffectiveTo(effectiveToDate);
		vendor.setInspect(vendorDTO.getInspect());
		//vendor.setInspectChargeId(vendorDTO.getInspectCharge());
		vendor.setPostCode(vendorDTO.getPostCode());
		//vendor.setReceiptChargeId(vendorDTO.getReceiptCharge());
		//vendor.setReportChargeIds(vendorDTO.getReportChargeIds());
		vendor.setStorageChargeDay(vendorDTO.getStorageChargeDay());
		//vendor.setStorageChargeId(vendorDTO.getStorageCharge());
		vendor.setStreet(vendorDTO.getStreet());
		vendor.setPostalTown(vendorDTO.getTown());
		//vendor.setTransportChargeId(vendorDTO.getTransportCharge());
		vendor.setUpdatedBy(vendorDTO.getUpdatedBy());
		vendor.setVatNumber(vendorDTO.getVatNumber());
		vendor.setVatPercentage(vendorDTO.getVatPercentage());
		vendor.setVendorReferenceCode(vendorDTO.getVendorCode());
		vendor.setVendorName(vendorDTO.getVendorName());
		vendor.setVersion(vendorDTO.getVersion() + 1);
		vendor.setAutoasnrequired(false);
		vendor.setControlVendor(false);
		vendor.setLocality(vendorDTO.getLocality());
		vendor.setActive(vendorDTO.getActive());
		vendor.setRecipient(vendorDTO.getRecipient() == null ? vendorDTO.getVendorName() : vendorDTO.getRecipient());
		
		return vendor;
	
    }

//	public static ChargeMaster convertChargeViewToEntity(ChargeDTO chargeDTO, ChargeMaster charge) {
//		
//		if(charge == null){
//		    charge = new ChargeMaster();
//		    charge.setDateCreated(new Date());
//			charge.setCreatedBy(chargeDTO.getCreatedBy());
//		}
//		charge.setChargeName(chargeDTO.getChargeName());
//		charge.setChargeType(chargeDTO.getChargeType());
//		charge.setStdCharge(chargeDTO.getStdCharge());
//		charge.setDateUpdated(new Date());
//		charge.setUpdatedBy(chargeDTO.getUpdatedBy());
//		return charge;
//	}

	public static LocationSubType convertZoneViewToEntity(ZoneTypeDTO zoneTypeDTO, LocationSubType zoneType) {
		
		if(zoneType == null){
			zoneType = new LocationSubType();
			zoneType.setDateCreated(new Date());
			zoneType.setCreatedBy(zoneTypeDTO.getCreatedBy());
		}
		
		zoneType.setDateUpdated(new Date());
		zoneType.setUpdatedBy(zoneTypeDTO.getUpdatedBy());
		zoneType.setLocationSubTypeCode(zoneTypeDTO.getZoneTypeCode());
		zoneType.setLocationSubTypeDescription(zoneTypeDTO.getZoneTypeDescription());
		zoneType.setVersion((long) 0);
		
		return zoneType;
	}

	public static LocationRow convertRowViewToEntity(RowDTO rowDTO, LocationRow row) {
		
		if(row == null){
			row = new LocationRow();
			row.setDateCreated(new Date());
			row.setCreatedBy(rowDTO.getCreatedBy());
		}
		row.setDateUpdated(new Date());
		row.setUpdatedBy(rowDTO.getUpdatedBy());
		row.setFillingPriority(rowDTO.getFillingPriority());
		row.setRowCode(rowDTO.getRowCode());
		row.setRowDescription(rowDTO.getRowDescription());
		
		return row;
	}

	public static LocationAisle convertAisleViewToEntity(AisleDTO aisleDTO, LocationAisle locationAisle) {
		
		if(locationAisle == null){
			locationAisle = new LocationAisle();
			locationAisle.setDateCreated(new Date());
			locationAisle.setCreatedBy(aisleDTO.getCreatedBy());
		}
		locationAisle.setDateUpdated(new Date());
		locationAisle.setUpdatedBy(aisleDTO.getUpdatedBy());
		locationAisle.setAisleCode(aisleDTO.getAisleCode());
		locationAisle.setAisleDescription(aisleDTO.getAisleDescription());
		locationAisle.setFillingPriority(aisleDTO.getFillingPriority());
		return locationAisle;
	}

	public static LocationColumn convertColumnViewToEntity(ColumnDTO columnDTO, LocationColumn locationColumn) {
		
		if(locationColumn == null){
			locationColumn = new LocationColumn();
			locationColumn.setDateCreated(new Date());
			locationColumn.setCreatedBy(columnDTO.getCreatedBy());;
		}
		locationColumn.setDateUpdated(new Date());
		locationColumn.setUpdatedBy(columnDTO.getUpdatedBy());
		locationColumn.setColumnCode(columnDTO.getColumnCode());
		locationColumn.setColumnDescription(columnDTO.getColumnDescription());
		locationColumn.setFillingPriority(columnDTO.getFillingPriority());
		
		return locationColumn;
	}

	public static LocationType convertLocationTypeViewToEntity(LocationTypeDTO locationTypeDTO, LocationType locationType) {
		if(locationType == null){
			locationType = new LocationType();
			locationType.setDateCreated(new Date());
			locationType.setCreatedBy(locationTypeDTO.getCreatedBy());
		}
		locationType.setDateUpdated(new Date());
		locationType.setUpdatedBy(locationTypeDTO.getUpdatedBy());
		locationType.setLocationTypeCode(locationTypeDTO.getLocationTypeCode());
		locationType.setLocationTypeDescription(locationTypeDTO.getLocationTypeDescription());
		//locationType.setLocationArea(locationTypeDTO.getLocationArea());
		locationType.setVersion((long) 0);
		locationType.setDefaultInventoryStatusId((long) 1);
		locationType.setIsTrain(locationTypeDTO.getIsTrain());
		locationType.setPlZone(locationTypeDTO.getPltZone());
		if(locationType.getPlZone()){
			locationType.setDropZoneLocationId(locationTypeDTO.getDropZoneId());
		}
		locationType.setIsPickable(locationTypeDTO.getIsPickable());
		locationType.setNoSerialScan(locationTypeDTO.getNoSerialScan());
		locationType.setActive(true);
		locationType.setLooseParts(false);
		locationType.setPickSmv(locationTypeDTO.getPickTime());
		return locationType;
	}

	public static Part convertPartViewToEntity(PartDTO partDTO, Part part) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		if(part == null){
			part = new Part();
			part.setDateCreated(new Date());
			part.setCreatedBy(partDTO.getCreatedBy());
		}
		
		part.setLastUpdated(new Date());
		part.setLastUpdatedBy(partDTO.getUpdatedBy());
		part.setAutoPick(partDTO.getAutoPick());
		part.setAutoReceive(partDTO.getAutoReceive());
		part.setUpdateSerialWithCount(partDTO.getCountOnRcpt());
		part.setRequiresDecant(partDTO.getDecant());
		//part.setDecantChargeId(partDTO.getDecantCharge());
		//part.setDespatchChargeId(partDTO.getDespatchCharge());
		Date effectiveFromDate = sdf.parse(partDTO.getEffectiveFrom());
		part.setEffectiveFrom(effectiveFromDate);
		Date effectiveToDate = sdf.parse(partDTO.getEffectiveTo());
		part.setEffectiveTo(effectiveToDate);
		part.setHighValue(partDTO.getHighValue());
		part.setImportantA(false);
		part.setRequiresInspection(partDTO.getInspect());
		part.setIncludeInventory(partDTO.getIncludeInventory());
	//	part.setInspectChargeId(partDTO.getInspectCharge());
		part.setLoosePartQty(partDTO.getLoosePcsEqualTo1()== null ? 0 :partDTO.getLoosePcsEqualTo1());
		part.setMiniStock(partDTO.getMiniStock());
		part.setPackType(partDTO.getPackType());
		part.setPartDescription(partDTO.getPartDescription());
		part.setPartFamily(partDTO.getPartFamily());
		part.setPartNumber(partDTO.getPartNumber());
		part.setRdtPrompt(partDTO.getRdtMessage());
	//	part.setReceiptChargeId(partDTO.getReceiptCharge());
		part.setShelfLife(partDTO.getShelfLife());
	//	part.setStorageChargeId(partDTO.getStorageCharge());
	//	part.setTransportChargeId(partDTO.getTransportCharge());
		part.setUpdateSerialWithCount(partDTO.getUpdateSerialWithCount());
		part.setJisSupplyGroup(partDTO.getJisSupplyGroup());
		part.setWiCode(partDTO.getWiCode());
		part.setWeight(partDTO.getWeight());
		part.setVersion((long) 0);
		part.setImaginaryPart(false);
		part.setSafetyStock(0);
		part.setVendor(partDTO.getVendor());
		part.setFullBoxPick(partDTO.getFullBoxPick());
		part.setModular(partDTO.getModular());
		part.setIsBigPart(partDTO.getBigPart());
		part.setIncludeInventory(0);
		part.setConversionFactor(1);
		part.setFixedLocationType(partDTO.getLocationType());
		part.setFixedSubType(partDTO.getLocationSubType());
		part.setPiecePartNumber(partDTO.getPiecePartNumber());
		part.setRequiresCount(partDTO.getCountOnRcpt());
		part.setIsAS400(partDTO.getIsAS400());
		part.setStackHeight(partDTO.getStackHeight());
		
		return part;
	}

//	public static ChargeType convertChargeTypeViewToEntity(ChargeTypeDTO chargeTypeDTO, ChargeType chargeType) {
//		if(chargeType == null){
//			chargeType = new ChargeType();
//			chargeType.setDateCreated(new Date());
//			chargeType.setCreatedBy(chargeTypeDTO.getCreatedBy());
//		}
//		chargeType.setDateUpdated(new Date());
//		chargeType.setUpdatedBy(chargeTypeDTO.getUpdatedBy());
//		chargeType.setChargeName(chargeTypeDTO.getChargeName());
//		chargeType.setChargeDescription(chargeTypeDTO.getChargeDescription());
//		return chargeType;
//	}

	public static LocationRack convertRackViewToEntity(RackDTO rackDTO, LocationRack rack) {
		if(rack == null){
			rack = new LocationRack();
			rack.setDateCreated(new Date());
			rack.setCreatedBy(rackDTO.getCreatedBy());
		}
		rack.setDateUpdated(new Date());
		rack.setUpdatedBy(rackDTO.getUpdatedBy());
		rack.setAisle(rackDTO.getAisle());
		rack.setFillingPriority(rackDTO.getFillingPriority());
		rack.setPriorityRowColumn(rackDTO.getPriorityRowColumn());
		rack.setRackCode(rackDTO.getRackCode());
		rack.setRackDescription(rackDTO.getRackDescription());
		
		return rack;
	}
	
}
