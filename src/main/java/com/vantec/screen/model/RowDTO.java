package com.vantec.screen.model;

public class RowDTO {
	
	
	private Long id;
	private String rowCode;
	private String rowDescription;
	private Integer fillingPriority;
	private String createdBy;
	private String updatedBy;


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRowCode() {
		return rowCode;
	}
	public void setRowCode(String rowCode) {
		this.rowCode = rowCode;
	}
	public String getRowDescription() {
		return rowDescription;
	}
	public void setRowDescription(String rowDescription) {
		this.rowDescription = rowDescription;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getFillingPriority() {
		return fillingPriority;
	}
	public void setFillingPriority(Integer fillingPriority) {
		this.fillingPriority = fillingPriority;
	}
	
	
	

}
