package com.vantec.screen.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class ZoneTypeDTO {
	
	private Long id;
	private String zoneTypeCode;
	private String zoneTypeDescription;
	private String createdBy;
	private String updatedBy;
	private Boolean pltZone;
	

	public Boolean getPltZone() {
		return pltZone;
	}
	public void setPltZone(Boolean isPltZone) {
		this.pltZone = isPltZone;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getZoneTypeCode() {
		return zoneTypeCode;
	}
	public void setZoneTypeCode(String zoneTypeCode) {
		this.zoneTypeCode = zoneTypeCode;
	}
	public String getZoneTypeDescription() {
		return zoneTypeDescription;
	}
	public void setZoneTypeDescription(String zoneTypeDescription) {
		this.zoneTypeDescription = zoneTypeDescription;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	

}
