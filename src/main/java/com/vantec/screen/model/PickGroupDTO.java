package com.vantec.screen.model;

public class PickGroupDTO {
	
	private Long id; //edit
	private String pickGroupCode;
	private String pickGroupDescription;
	private Boolean pallet;
	private Integer maxCaseQty;
	private String userId;
	private String altPickGroup;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPickGroupCode() {
		return pickGroupCode;
	}
	public void setPickGroupCode(String pickGroupCode) {
		this.pickGroupCode = pickGroupCode;
	}
	public String getPickGroupDescription() {
		return pickGroupDescription;
	}
	public Boolean getPallet() {
		return pallet;
	}
	public void setPallet(Boolean pallet) {
		this.pallet = pallet;
	}
	public void setPickGroupDescription(String pickGroupDescription) {
		this.pickGroupDescription = pickGroupDescription;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getMaxCaseQty() {
		return maxCaseQty;
	}
	public void setMaxCaseQty(Integer maxCaseQty) {
		this.maxCaseQty = maxCaseQty;
	}
	public String getAltPickGroup() {
		return altPickGroup;
	}
	public void setAltPickGroup(String altPickGroup) {
		this.altPickGroup = altPickGroup;
	}
	
	
   
	

}
