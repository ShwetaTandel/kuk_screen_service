package com.vantec.screen.model;

public class LocationFillingStatusDTO {
	
	private Long fillingStatusId; //edit
	private String fillingCode;
	private String fillingDescription;
	private boolean pick;
	private boolean putaway;
	private String userId;
	
	
	public Long getFillingStatusId() {
		return fillingStatusId;
	}
	public void setFillingStatusId(Long fillingStatusId) {
		this.fillingStatusId = fillingStatusId;
	}
	public String getFillingCode() {
		return fillingCode;
	}
	public void setFillingCode(String fillingCode) {
		this.fillingCode = fillingCode;
	}
	public String getFillingDescription() {
		return fillingDescription;
	}
	public void setFillingDescription(String fillingDescription) {
		this.fillingDescription = fillingDescription;
	}
	public boolean isPick() {
		return pick;
	}
	public void setPick(boolean pick) {
		this.pick = pick;
	}
	public boolean isPutaway() {
		return putaway;
	}
	public void setPutaway(boolean putaway) {
		this.putaway = putaway;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	
	
   
	
	
	

}
