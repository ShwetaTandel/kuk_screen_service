package com.vantec.screen.model;

import com.vantec.screen.entity.LocationAisle;

public class RackDTO {
	
	private Long id;
	private String rackCode;
	private String rackDescription;
	private Long aisleId;
	private LocationAisle aisle;
	private Long fillingPriority;
	private String priorityRowColumn;
	private String createdBy;
	private String updatedBy;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRackCode() {
		return rackCode;
	}
	public void setRackCode(String rackCode) {
		this.rackCode = rackCode;
	}
	public String getRackDescription() {
		return rackDescription;
	}
	public void setRackDescription(String rackDescription) {
		this.rackDescription = rackDescription;
	}
	public Long getAisleId() {
		return aisleId;
	}
	public void setAisleId(Long aisleId) {
		this.aisleId = aisleId;
	}
	public LocationAisle getAisle() {
		return aisle;
	}
	public void setAisle(LocationAisle aisle) {
		this.aisle = aisle;
	}
	public Long getFillingPriority() {
		return fillingPriority;
	}
	public void setFillingPriority(Long fillingPriority) {
		this.fillingPriority = fillingPriority;
	}
	public String getPriorityRowColumn() {
		return priorityRowColumn;
	}
	public void setPriorityRowColumn(String priorityRowColumn) {
		this.priorityRowColumn = priorityRowColumn;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	
	
	
	
	
	

}
