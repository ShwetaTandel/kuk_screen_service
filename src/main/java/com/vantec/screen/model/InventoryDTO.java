package com.vantec.screen.model;

public class InventoryDTO {
	
	private String partNumber;
	private String serialNumber;
	private String statusCode;
	private Boolean decant;
	private Boolean inspect;
	private String currentUser;
	private String locationCode;
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public Boolean getDecant() {
		return decant;
	}
	public void setDecant(Boolean decant) {
		this.decant = decant;
	}
	public Boolean getInspect() {
		return inspect;
	}
	public void setInspect(Boolean inspect) {
		this.inspect = inspect;
	}
	public String getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
    
	
	
	
	
	

}
