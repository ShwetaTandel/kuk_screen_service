package com.vantec.screen.model;

public class ManifestDTO {
	
	
	private String documentReference;
	private String expectedDeliverTime;
	private String updatedBy;
	
	public String getDocumentReference() {
		return documentReference;
	}
	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}
	public String getExpectedDeliverTime() {
		return expectedDeliverTime;
	}
	public void setExpectedDeliverTime(String expectedDeliverTime) {
		this.expectedDeliverTime = expectedDeliverTime;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	

}
