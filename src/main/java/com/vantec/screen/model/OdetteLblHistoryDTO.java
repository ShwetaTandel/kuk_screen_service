package com.vantec.screen.model;

public class OdetteLblHistoryDTO {
	
	private String ranOrder;
	private String partNumber;
	private Integer snp;
	private Integer noOfLabel;
	private String createdBy;
	private String updatedBy;
	
	public String getRanOrder() {
		return ranOrder;
	}
	public void setRanOrder(String ranOrder) {
		this.ranOrder = ranOrder;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Integer getSnp() {
		return snp;
	}
	public void setSnp(Integer snp) {
		this.snp = snp;
	}
	public Integer getNoOfLabel() {
		return noOfLabel;
	}
	public void setNoOfLabel(Integer noOfLabel) {
		this.noOfLabel = noOfLabel;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
    
	
	
	

}
