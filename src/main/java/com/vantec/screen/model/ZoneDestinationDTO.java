package com.vantec.screen.model;

public class ZoneDestinationDTO {
	
	private Long id;
	private String zoneDestinationCode;
	private String zoneDestinationDescription;
	private Long trolleyId;
	private String createdBy;
	private String updatedBy;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getZoneDestinationCode() {
		return zoneDestinationCode;
	}
	public void setZoneDestinationCode(String zoneDestinationCode) {
		this.zoneDestinationCode = zoneDestinationCode;
	}
	public String getZoneDestinationDescription() {
		return zoneDestinationDescription;
	}
	public void setZoneDestinationDescription(String zoneDestinationDescription) {
		this.zoneDestinationDescription = zoneDestinationDescription;
	}
	public Long getTrolleyId() {
		return trolleyId;
	}
	public void setTrolleyId(Long trolleyId) {
		this.trolleyId = trolleyId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
