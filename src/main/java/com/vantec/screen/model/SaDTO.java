package com.vantec.screen.model;

public class SaDTO {
	
	private String partNumber;
	private String serialReference;
	private Double qty;
	private String createdBy;
	private String reasonCode;
	private Boolean sendMsg;
	
	
	
	public Boolean getSendMsg() {
		return sendMsg;
	}
	public void setSendMsg(Boolean sendMsg) {
		this.sendMsg = sendMsg;
	}
	//Sa-Out
	private String fromLocationCode;
	
	
	
	//Sa-In
	private String toLocationCode;
	private String ranOrder;
	private String comment;
	
	
	//Other
	private String shortCode;
	
	
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getToLocationCode() {
		return toLocationCode;
	}
	public void setToLocationCode(String toLocationCode) {
		this.toLocationCode = toLocationCode;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getRanOrder() {
		return ranOrder;
	}
	public void setRanOrder(String ranOrder) {
		this.ranOrder = ranOrder;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getFromLocationCode() {
		return fromLocationCode;
	}
	public void setFromLocationCode(String fromLocationCode) {
		this.fromLocationCode = fromLocationCode;
	}
	
	
	
	
	
	

}
