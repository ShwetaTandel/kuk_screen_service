package com.vantec.screen.model;

public class AisleDTO {
	
	private Long id;
	private String aisleCode;
	private String aisleDescription;
	private Long fillingPriority;
	private String createdBy;
	private String updatedBy;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAisleCode() {
		return aisleCode;
	}
	public void setAisleCode(String aisleCode) {
		this.aisleCode = aisleCode;
	}
	public String getAisleDescription() {
		return aisleDescription;
	}
	public void setAisleDescription(String aisleDescription) {
		this.aisleDescription = aisleDescription;
	}
	public Long getFillingPriority() {
		return fillingPriority;
	}
	public void setFillingPriority(Long fillingPriority) {
		this.fillingPriority = fillingPriority;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	

}
