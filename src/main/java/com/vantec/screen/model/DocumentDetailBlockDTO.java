package com.vantec.screen.model;

public class DocumentDetailBlockDTO {
	
	private Long docDetailId;
	private String updatedBy;
	private String blockedComment;
	private String isBlock;
	
	public Long getDocDetailId() {
		return docDetailId;
	}
	public void setDocDetailId(Long docDetailId) {
		this.docDetailId = docDetailId;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getBlockedComment() {
		return blockedComment;
	}
	public void setBlockedComment(String blockedComment) {
		this.blockedComment = blockedComment;
	}
	public String getIsBlock() {
		return isBlock;
	}
	public void setIsBlock(String isBlock) {
		this.isBlock = isBlock;
	}
    
	
}
