package com.vantec.screen.model;

public class ColumnDTO {
	
	
	private Long id;
	private String columnCode;
	private String columnDescription;
	private Long fillingPriority;
	private String createdBy;
	private String updatedBy;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getColumnCode() {
		return columnCode;
	}
	public void setColumnCode(String columnCode) {
		this.columnCode = columnCode;
	}
	public String getColumnDescription() {
		return columnDescription;
	}
	public void setColumnDescription(String columnDescription) {
		this.columnDescription = columnDescription;
	}
	public Long getFillingPriority() {
		return fillingPriority;
	}
	public void setFillingPriority(Long fillingPriority) {
		this.fillingPriority = fillingPriority;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	
	
	

}
