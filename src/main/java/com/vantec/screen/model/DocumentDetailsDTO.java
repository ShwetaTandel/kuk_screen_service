package com.vantec.screen.model;

public class DocumentDetailsDTO {
	
	private Long docDetailId;
	private Double transactedQty;
	private String updatedBy;
	
	public Long getDocDetailId() {
		return docDetailId;
	}
	public void setDocDetailId(Long docDetailId) {
		this.docDetailId = docDetailId;
	}
    
	public Double getTransactedQty() {
		return transactedQty;
	}
	public void setTransactedQty(Double transactedQty) {
		this.transactedQty = transactedQty * 1000;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	
	
	
	
	
	
	public static void main(String args[]){
		double transactedQty = 1987;
		int precision = 1000; //keep 4 digits
		transactedQty = transactedQty * precision;
		Double newData = new Double(transactedQty); 
		Integer intNum = newData.intValue();
		
	}
	
	
	
}
