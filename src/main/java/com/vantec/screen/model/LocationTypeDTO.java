package com.vantec.screen.model;

public class LocationTypeDTO {
	
	private Long id;
	private String locationTypeCode;
	private String locationTypeDescription;
	private String locationArea;
	private Boolean isTrain;
	private Boolean isPickable;
	private Boolean pltZone;
	private Boolean noSerialScan;
	private Long dropZoneId;
	private Long pickTime;
	private String createdBy;
	private String updatedBy;
	
	public Long getPickTime() {
		return pickTime;
	}
	public void setPickTime(Long pickTime) {
		this.pickTime = pickTime;
	}
	public Boolean getNoSerialScan() {
		return noSerialScan;
	}
	public void setNoSerialScan(Boolean noSerialScan) {
		this.noSerialScan = noSerialScan;
	}
	public Boolean getIsPickable() {
		return isPickable;
	}
	public void setIsPickable(Boolean isPickable) {
		this.isPickable = isPickable;
	}
	public Boolean getPltZone() {
		return pltZone;
	}
	public void setPltZone(Boolean pltZone) {
		this.pltZone = pltZone;
	}
	public Long getDropZoneId() {
		return dropZoneId;
	}
	public void setDropZoneId(Long dropZoneId) {
		this.dropZoneId = dropZoneId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLocationTypeCode() {
		return locationTypeCode;
	}
	public void setLocationTypeCode(String locationTypeCode) {
		this.locationTypeCode = locationTypeCode;
	}
	public String getLocationTypeDescription() {
		return locationTypeDescription;
	}
	public void setLocationTypeDescription(String locationTypeDescription) {
		this.locationTypeDescription = locationTypeDescription;
	}
	public String getLocationArea() {
		return locationArea;
	}
	public void setLocationArea(String locationArea) {
		this.locationArea = locationArea;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Boolean getIsTrain() {
		return isTrain;
	}
	public void setIsTrain(Boolean isTrain) {
		this.isTrain = isTrain;
	}
	
	
	
	
	

}
