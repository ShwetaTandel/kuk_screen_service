package com.vantec.screen.model;

public class TrolleyDTO {
	
	private Long id;
	private String trolleyCode;
	private String trolleyDescription;
	private Long trainId;
	private String createdBy;
	private String updatedBy;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTrolleyCode() {
		return trolleyCode;
	}
	public void setTrolleyCode(String trolleyCode) {
		this.trolleyCode = trolleyCode;
	}
	public String getTrolleyDescription() {
		return trolleyDescription;
	}
	public void setTrolleyDescription(String torlleyDescription) {
		this.trolleyDescription = torlleyDescription;
	}
	public Long getTrainId() {
		return trainId;
	}
	public void setTrainId(Long trainId) {
		this.trainId = trainId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	

}
