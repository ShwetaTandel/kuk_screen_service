package com.vantec.screen.model;

public class SplitPieceDTO {
	
	private String partNumber;
	private String serial1;
	private String serial2;
	private Double qty;
	private String createdBy;
	private String toLocationCode;
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getSerial1() {
		return serial1;
	}
	public void setSerial1(String serial1) {
		this.serial1 = serial1;
	}
	public String getSerial2() {
		return serial2;
	}
	public void setSerial2(String serial2) {
		this.serial2 = serial2;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getToLocationCode() {
		return toLocationCode;
	}
	public void setToLocationCode(String toLocationCode) {
		this.toLocationCode = toLocationCode;
	}
	
	
	

	
	

	
	

}
