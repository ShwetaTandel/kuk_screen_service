package com.vantec.screen.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class LocationDTO {
	
	private Long id;
	private char startAisle;
	private char endAisle;
	private char startRack;
	private char endRack;
	private Integer startColumn;
	private Integer endColumn;
	private char startRow;
	private char endRow;
	private Boolean locationFillingStatus;
	private Double maxWeight;
	private String partNumber;
	private Integer maxQty;
	private Long pickGroupId;
	
	
	private Long locationTypeId;
	private Long zoneTypeId;
	private Long productTypeId;
	private Long inventoryStatusId;
	private Boolean multiPart;
	private Boolean replenWhenEmpty;
	private Long locationFillingStatusId;
	
	private String createdBy;
	private String updatedBy;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public char getStartAisle() {
		return startAisle;
	}
	public void setStartAisle(char startAisle) {
		this.startAisle = startAisle;
	}
	public char getEndAisle() {
		return endAisle;
	}
	public void setEndAisle(char endAisle) {
		this.endAisle = endAisle;
	}
	public char getStartRack() {
		return startRack;
	}
	public void setStartRack(char startRack) {
		this.startRack = startRack;
	}
	public char getEndRack() {
		return endRack;
	}
	public void setEndRack(char endRack) {
		this.endRack = endRack;
	}
	public Integer getStartColumn() {
		return startColumn;
	}
	public void setStartColumn(Integer startColumn) {
		this.startColumn = startColumn;
	}
	public Integer getEndColumn() {
		return endColumn;
	}
	public void setEndColumn(Integer endColumn) {
		this.endColumn = endColumn;
	}
	public char getStartRow() {
		return startRow;
	}
	public void setStartRow(char startRow) {
		this.startRow = startRow;
	}
	public char getEndRow() {
		return endRow;
	}
	public void setEndRow(char endRow) {
		this.endRow = endRow;
	}
	public Long getLocationTypeId() {
		return locationTypeId;
	}
	public void setLocationTypeId(Long locationTypeId) {
		this.locationTypeId = locationTypeId;
	}
	public Long getZoneTypeId() {
		return zoneTypeId;
	}
	public void setZoneTypeId(Long zoneTypeId) {
		this.zoneTypeId = zoneTypeId;
	}
	public Long getProductTypeId() {
		return productTypeId;
	}
	public void setProductTypeId(Long productTypeId) {
		this.productTypeId = productTypeId;
	}
	public Long getInventoryStatusId() {
		return inventoryStatusId;
	}
	public void setInventoryStatusId(Long inventoryStatusId) {
		this.inventoryStatusId = inventoryStatusId;
	}
	public Boolean getMultiPart() {
		return multiPart;
	}
	public void setMultiPart(Boolean multiPart) {
		this.multiPart = multiPart;
	}
	public Boolean getReplenWhenEmpty() {
		return replenWhenEmpty;
	}
	public void setReplenWhenEmpty(Boolean replenWhenEmpty) {
		this.replenWhenEmpty = replenWhenEmpty;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Boolean getLocationFillingStatus() {
		return locationFillingStatus;
	}
	public void setLocationFillingStatus(Boolean locationFillingStatus) {
		this.locationFillingStatus = locationFillingStatus;
	}
	public Long getLocationFillingStatusId() {
		return locationFillingStatusId;
	}
	public void setLocationFillingStatusId(Long locationFillingStatusId) {
		this.locationFillingStatusId = locationFillingStatusId;
	}
	public Double getMaxWeight() {
		return maxWeight;
	}
	public void setMaxWeight(Double maxWeight) {
		this.maxWeight = maxWeight;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Integer getMaxQty() {
		return maxQty;
	}
	public void setMaxQty(Integer maxQty) {
		this.maxQty = maxQty;
	}
	public Long getPickGroupId() {
		return pickGroupId;
	}
	public void setPickGroupId(Long pickGroupId) {
		this.pickGroupId = pickGroupId;
	}
	
    
	
	
	

}
