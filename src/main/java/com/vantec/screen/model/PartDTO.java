package com.vantec.screen.model;

import com.vantec.screen.entity.LocationSubType;
import com.vantec.screen.entity.LocationType;
import com.vantec.screen.entity.PackType;
import com.vantec.screen.entity.Vendor;

public class PartDTO {
	
	private Long id;
	private String partNumber;
	private String effectiveFrom;
	private String effectiveTo;
	private PackType packType;
	private Long packTypeId;
	private String partDescription;
	private Vendor vendor;
	private Long vendorId;
	private String jisSupplyGroup;
	private String wiCode;
	private String fixedLocationCode;
	private Long locationTypeId;
	private LocationType locationType;
	private Long zoneTypeId;
	private LocationSubType locationSubType;
	private Boolean countOnRcpt;
	private Boolean updateSerialWithCount;
	private Boolean decant;
	private Boolean inspect;
	private Integer includeInventory = 0;
	private Integer miniStock;
	private Integer shelfLife;
	private Boolean highValue;
	private Boolean autoReceive;
	private Boolean autoPick;
	private Boolean fullBoxPick;
	private String rdtMessage;
	private String partFamily;
	private Integer loosePcsEqualTo1;
	//private ChargeMaster storageCharge;
	private Long storageChargeId;
	//private ChargeMaster receiptCharge;
	private Long receiptChargeId;
	//private ChargeMaster despatchCharge;
	private Long despatchChargeId;
	//private ChargeMaster decantCharge;
	private Long decantChargeId;
	//private ChargeMaster inspectCharge;
	private Long inspectChargeId;
	//private ChargeMaster transportCharge;
	private Long transportChargeId;
	private String createdBy;
	private String updatedBy;
	private Double weight;
	private String piecePartNumber;
	private Boolean modular;
	private Boolean bigPart;
	private Boolean isAS400 =false;
	private Integer stackHeight;
	
	public Integer getStackHeight() {
		return stackHeight;
	}
	public void setStackHeight(Integer stackHeight) {
		this.stackHeight = stackHeight;
	}
	public Boolean getIsAS400() {
		return isAS400;
	}
	public void setIsAS400(Boolean isAS400) {
		this.isAS400 = isAS400;
	}
	public Boolean getBigPart() {
		return bigPart;
	}
	public void setBigPart(Boolean bigPart) {
		this.bigPart = bigPart;
	}
	public Boolean getModular() {
		return modular;
	}
	public void setModular(Boolean modular) {
		this.modular = modular;
	}
	public Boolean getFullBoxPick() {
		return fullBoxPick;
	}
	public void setFullBoxPick(Boolean fullBoxPick) {
		this.fullBoxPick = fullBoxPick;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getEffectiveFrom() {
		return effectiveFrom;
	}
	public void setEffectiveFrom(String effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}
	public String getEffectiveTo() {
		return effectiveTo;
	}
	public void setEffectiveTo(String effectiveTo) {
		this.effectiveTo = effectiveTo;
	}
	public PackType getPackType() {
		return packType;
	}
	public void setPackType(PackType packType) {
		this.packType = packType;
	}
	public Long getPackTypeId() {
		return packTypeId;
	}
	public void setPackTypeId(Long packTypeId) {
		this.packTypeId = packTypeId;
	}
	public String getPartDescription() {
		return partDescription;
	}
	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}
	public Vendor getVendor() {
		return vendor;
	}
	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
	public Long getVendorId() {
		return vendorId;
	}
	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}
	public String getWiCode() {
		return wiCode;
	}
	public void setWiCode(String wiCode) {
		this.wiCode = wiCode;
	}
	public String getFixedLocationCode() {
		return fixedLocationCode;
	}
	public void setFixedLocationCode(String fixedLocationCode) {
		this.fixedLocationCode = fixedLocationCode;
	}
	public Long getLocationTypeId() {
		return locationTypeId;
	}
	public void setLocationTypeId(Long locationTypeId) {
		this.locationTypeId = locationTypeId;
	}
	public Long getZoneTypeId() {
		return zoneTypeId;
	}
	public void setZoneTypeId(Long zoneTypeId) {
		this.zoneTypeId = zoneTypeId;
	}
	public Boolean getCountOnRcpt() {
		return countOnRcpt;
	}
	public void setCountOnRcpt(Boolean countOnRcpt) {
		this.countOnRcpt = countOnRcpt;
	}
	public Boolean getUpdateSerialWithCount() {
		return updateSerialWithCount;
	}
	public void setUpdateSerialWithCount(Boolean updateSerialWithCount) {
		this.updateSerialWithCount = updateSerialWithCount;
	}
	public Boolean getDecant() {
		return decant;
	}
	public void setDecant(Boolean decant) {
		this.decant = decant;
	}
	public Boolean getInspect() {
		return inspect;
	}
	public void setInspect(Boolean inspect) {
		this.inspect = inspect;
	}
	public Integer getMiniStock() {
		return miniStock;
	}
	public void setMiniStock(Integer miniStock) {
		this.miniStock = miniStock;
	}
	public Integer getShelfLife() {
		return shelfLife;
	}
	public void setShelfLife(Integer shelfLife) {
		this.shelfLife = shelfLife;
	}
	
	public Boolean getHighValue() {
		return highValue;
	}
	public void setHighValue(Boolean highValue) {
		this.highValue = highValue;
	}
	public Boolean getAutoReceive() {
		return autoReceive;
	}
	public void setAutoReceive(Boolean autoReceive) {
		this.autoReceive = autoReceive;
	}
	public Boolean getAutoPick() {
		return autoPick;
	}
	public void setAutoPick(Boolean autoPick) {
		this.autoPick = autoPick;
	}
	public String getRdtMessage() {
		return rdtMessage;
	}
	public void setRdtMessage(String rdtMessage) {
		this.rdtMessage = rdtMessage;
	}
	public String getPartFamily() {
		return partFamily;
	}
	public void setPartFamily(String partFamily) {
		this.partFamily = partFamily;
	}
	public Integer getLoosePcsEqualTo1() {
		return loosePcsEqualTo1;
	}
	public void setLoosePcsEqualTo1(Integer loosePcsEqualTo1) {
		this.loosePcsEqualTo1 = loosePcsEqualTo1;
	}
//	public ChargeMaster getStorageCharge() {
//		return storageCharge;
//	}
//	public void setStorageCharge(ChargeMaster storageCharge) {
//		this.storageCharge = storageCharge;
//	}
	public Long getStorageChargeId() {
		return storageChargeId;
	}
	public void setStorageChargeId(Long storageChargeId) {
		this.storageChargeId = storageChargeId;
	}
//	public ChargeMaster getReceiptCharge() {
//		return receiptCharge;
//	}
//	public void setReceiptCharge(ChargeMaster receiptCharge) {
//		this.receiptCharge = receiptCharge;
//	}
	public Long getReceiptChargeId() {
		return receiptChargeId;
	}
	public void setReceiptChargeId(Long receiptChargeId) {
		this.receiptChargeId = receiptChargeId;
	}
//	public ChargeMaster getDespatchCharge() {
//		return despatchCharge;
//	}
//	public void setDespatchCharge(ChargeMaster despatchCharge) {
//		this.despatchCharge = despatchCharge;
//	}
	public Long getDespatchChargeId() {
		return despatchChargeId;
	}
	public void setDespatchChargeId(Long despatchChargeId) {
		this.despatchChargeId = despatchChargeId;
	}
//	public ChargeMaster getDecantCharge() {
//		return decantCharge;
//	}
//	public void setDecantCharge(ChargeMaster decantCharge) {
//		this.decantCharge = decantCharge;
//	}
	public Long getDecantChargeId() {
		return decantChargeId;
	}
	public void setDecantChargeId(Long decantChargeId) {
		this.decantChargeId = decantChargeId;
	}
//	public ChargeMaster getInspectCharge() {
//		return inspectCharge;
//	}
//	public void setInspectCharge(ChargeMaster inspectCharge) {
//		this.inspectCharge = inspectCharge;
//	}
	public Long getInspectChargeId() {
		return inspectChargeId;
	}
	public void setInspectChargeId(Long inspectChargeId) {
		this.inspectChargeId = inspectChargeId;
	}
//	public ChargeMaster getTransportCharge() {
//		return transportCharge;
//	}
//	public void setTransportCharge(ChargeMaster transportCharge) {
//		this.transportCharge = transportCharge;
//	}
	public Long getTransportChargeId() {
		return transportChargeId;
	}
	public void setTransportChargeId(Long transportChargeId) {
		this.transportChargeId = transportChargeId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public LocationType getLocationType() {
		return locationType;
	}
	public void setLocationType(LocationType locationType) {
		this.locationType = locationType;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public LocationSubType getLocationSubType() {
		return locationSubType;
	}
	public void setLocationSubType(LocationSubType locationSubType) {
		this.locationSubType = locationSubType;
	}
	public Integer getIncludeInventory() {
		return includeInventory;
	}
	public void setIncludeInventory(Integer includeInventory) {
		this.includeInventory = includeInventory;
	}
	public String getPiecePartNumber() {
		return piecePartNumber;
	}
	public void setPiecePartNumber(String piecePartNumber) {
		this.piecePartNumber = piecePartNumber;
	}
	public String getJisSupplyGroup() {
		return jisSupplyGroup;
	}
	public void setJisSupplyGroup(String jisSupplyGroup) {
		this.jisSupplyGroup = jisSupplyGroup;
	}
	
	
	
	
	
	
	
	

}
