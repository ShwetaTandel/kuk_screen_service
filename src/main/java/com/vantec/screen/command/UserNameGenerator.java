package com.vantec.screen.command;

public class UserNameGenerator implements CustomValueGeneratorCommand<String>{
	
	

	public UserNameGenerator() {
	}

	public String generate(Long id,String userTypeName) {

		String prefix = userTypeName;

		int maxvalue = 3;
		String userRef = "";
		int numberOfDigits = 0;
		numberOfDigits = String.valueOf(id).length();
		for (int i = 0; i < maxvalue - numberOfDigits; i++) {
			userRef += "0";
		}
		userRef = prefix + userRef + String.valueOf(id);
		return userRef;

	}

}
