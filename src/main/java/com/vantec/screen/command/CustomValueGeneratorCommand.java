package com.vantec.screen.command;

public interface CustomValueGeneratorCommand<T> {

	public T generate(Long id,String name);
}
