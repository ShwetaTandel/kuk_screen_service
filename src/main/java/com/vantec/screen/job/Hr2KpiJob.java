package com.vantec.screen.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.vantec.screen.service.Hr2KpiService;  

@Component
public class Hr2KpiJob {
	
	@Autowired
	Hr2KpiService hr2KpiService;
	
	//@Scheduled(cron = "0 0 0/2 * * *")
	public void updateKPIData(){
		
		hr2KpiService.updateKPILocationCount();
		
		hr2KpiService.updateKPIreplenCount();
		
		hr2KpiService.updatePickLocationCount();
		
	}
	
}
