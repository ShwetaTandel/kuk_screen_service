package com.vantec.screen.job;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vantec.screen.entity.Company;
import com.vantec.screen.repository.CompanyRepository;
import com.vantec.screen.service.MailService;  

@Component
public class SendReportDailyJob {
	
	private static Logger logger = LoggerFactory.getLogger(SendReportDailyJob.class);
	
	@Autowired
	MailService mailService;
	
	@Autowired
	CompanyRepository companyRepository;
	
	//@Scheduled(cron = "0 45 06 ? * *")
	public void generetaAmReport() throws IOException{
		logger.info("Generate Morning Report");
		generateReport();
	}
	
	//@Scheduled(cron = "0 45 14 ? * *")
	public void generetaPmReport() throws IOException{
		logger.info("Generate Evening Report");
		generateReport();
	}
		
	
	private void generateReport() throws IOException{
		
		
		
				Company company = getComapny();
				
				Date date = new Date();
				SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
				String formattedTime = timeFormat.format(date);
				String currentTime = formattedTime.replaceAll(":", "");
				
				
				
				
				// dayshift reports
				if (currentTime.equalsIgnoreCase("0630")) {
					
					
					
					//current time
					currentTime = currentTime + "am";
					
					//previous time
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY,14);
					cal.set(Calendar.MINUTE,30);
					cal.set(Calendar.SECOND,0);
					Date prevTime = cal.getTime();
					prevTime.setDate(prevTime.getDate()-1);
					
					//run reports
					
				}
				
				// backshift reports
				if (currentTime.equalsIgnoreCase("1430")) {
					
					//current time
					currentTime = currentTime + "pm";
					
					//previous time
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY,06);
					cal.set(Calendar.MINUTE,30);
					cal.set(Calendar.SECOND,0);
					Date prevTime = cal.getTime();
					
					//run reports
				}

 
    }  
	
	private Company getComapny(){
		List<Company> companys =  companyRepository.findAll();
		return companys.get(0);
	 }
	
	
	public static void main(String args[]){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date date = new Date();
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY,12);
		cal.set(Calendar.MINUTE,30);
		cal.set(Calendar.SECOND,0);
		Date prevTime = cal.getTime();
		prevTime.setDate(prevTime.getDate()-1);
		
		String prodStmp = "10-12-21 08:00";
		String year = "20"+  prodStmp.substring(6,8);
		String month = prodStmp.substring(3,5);
		String date1 = prodStmp.substring(0,2);
		String time = prodStmp.substring(9,11) +prodStmp.substring(12);
		System.out.println(year+month+date1+time);
		
		
		
		
	}
	
	
	

	

	
}
