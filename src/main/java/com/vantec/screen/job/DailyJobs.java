package com.vantec.screen.job;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.vantec.screen.entity.Company;
import com.vantec.screen.entity.JobLog;
import com.vantec.screen.repository.CompanyRepository;
import com.vantec.screen.repository.JobLogRepository;
import com.vantec.screen.repository.PartRepository;
import com.vantec.screen.service.JobService;

@Component
public class DailyJobs {

	private static Logger logger = LoggerFactory.getLogger(DailyJobs.class);

	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	PartRepository partRepository;

	@Autowired
	JobLogRepository jobLogRepository;

	@Autowired
	JobService jobService;

	/*
	 * 
	 * Daily Parts file job - Runs at night 1 am
	 */
	// @Scheduled(fixedRate = 5000)
	//second, minute, hour, day, month, weekday
	@Scheduled(cron = "0 0 1 * * ?")
	public void readPartFile() {
		
		String inpPath = getAdHocPath() + "\\parts";
		String tmpPath = getTempPath();
		File file = new File(inpPath);
		if (file.exists()) {
			JobLog log = recordJobLog("parts", "PROCESSING", "Daily Part File");
			try {

				logger.info("Reading file " + file.getName());
				try {
					jobService.readDailyPartFile(file);
					deleteFile(file);
					log.setStatus("SUCCESS");
					log.setJobEnded(new Date());
				} catch (Exception ex) {
					log.setStatus("FAILED");
					log.setJobEnded(new Date());
					file.renameTo(new File(tmpPath + "/" + file.getName()));
					logger.error("Moving file to temp folder due to error");
					logger.error(ex.getMessage(), ex);
					throw new DataIntegrityViolationException(ex.getMessage());
				}
			} catch (Exception e) {

				log.setStatus("FAILED");
				log.setJobEnded(new Date());
				logger.error("File not found");
				e.printStackTrace();
			}
			jobLogRepository.save(log);
		}
	}

	/*
	 * 
	 * Daily Receiptplan file job - Runs at morning 5 am
	 */
	// @Scheduled(fixedRate = 5000)
	@Scheduled(cron = "0 0 5 * * ?")
	public void readReceiptPlanFile() {
		String inpPath = getAdHocPath() + "\\RECEIPTPLAN";
		String tmpPath = getTempPath();
		File file = new File(inpPath);
		if (file.exists()) {
			JobLog log = recordJobLog("RECEIPTPLAN", "PROCESSING", "Daily ReceiptPlan File");
			try {

				logger.info("Reading file " + file.getName());
				try {
					jobService.readReceiptPlanFile(file);
					deleteFile(file);
					log.setStatus("SUCCESS");
					log.setJobEnded(new Date());
				} catch (Exception ex) {
					file.renameTo(new File(tmpPath + "/" + file.getName()));
					log.setStatus("FAILED");
					log.setJobEnded(new Date());
					logger.error("Moving file to temp folder due to error");
					logger.error(ex.getMessage(), ex);
					throw new DataIntegrityViolationException(ex.getMessage());
				}
			} catch (Exception e) {
				logger.error("File not found");
				log.setStatus("FAILED");
				log.setJobEnded(new Date());
				logger.error(e.getMessage(), e);
			}
			jobLogRepository.save(log);
		}
	}

//	/*
//	 * 
//	 * Daily Parts file job - Runs at night 1 am
//	 */
//	// @Scheduled(fixedRate = 5000)
//	//@Scheduled(cron = "0 */20 4-8 * * *")
//	public void readPickPlanFile() {
//
//		String inpPath = getAdHocPath() + "\\PICKPLAN.txt";
//		String tmpPath = getTempPath();
//		File file = new File(inpPath);
//		if (file.exists()) {
//			JobLog log = recordJobLog("PICKPLAN.txt", "PROCESSING", "Daily PickPlan File");
//			try {
//
//				logger.info("Reading file " + file.getName());
//				try {
//
//					jobService.readPickPlanFile(file);
//					deleteFile(file);
//					log.setStatus("SUCCESS");
//					log.setJobEnded(new Date());
//
//				} catch (Exception e) {
//					file.renameTo(new File(tmpPath + "\\" + "PICKPLAN.txt"));
//					logger.error("Moving file to temp folder due to error");
//					logger.error("File not found");
//					log.setStatus("FAILED");
//					log.setJobEnded(new Date());
//					logger.error(e.getMessage(), e);
//				}
//			} catch (Exception e) {
//				logger.error("File not found");
//				log.setStatus("FAILED");
//				log.setJobEnded(new Date());
//				logger.error(e.getMessage(), e);
//			}
//			jobLogRepository.save(log);
//		}
//	}
//	
	
	
	/*
	 * 
	 * Daily Parts file job - Runs at night 1 am
	 */
	// @Scheduled(fixedRate = 5000)
	//@Scheduled(cron = "0 */20 4-8 * * *")
	public void readReconFile() {

		String inpPath = getAdHocPath();
		String tmpPath = getTempPath();
		List<File> filesInFolder;
		try {
			filesInFolder = Files.walk(Paths.get(inpPath)).filter(Files::isRegularFile).map(Path::toFile)
					.collect(Collectors.toList());
		
		filesInFolder = filesInFolder.stream().filter(file -> file.getName().startsWith("recon")).filter(file -> file.getName().endsWith(".txt"))
				.collect(Collectors.toList());
		for (File file : filesInFolder) {
			JobLog log = recordJobLog(file.getName(), "PROCESSING", "Daily Recon File");
			try {

				logger.info("Reading file " + file.getName());
				try {

					jobService.readPickPlanFile(file);
					deleteFile(file);
					log.setStatus("SUCCESS");
					log.setJobEnded(new Date());

				} catch (Exception e) {
					file.renameTo(new File(tmpPath + "\\" + "PICKPLAN.txt"));
					logger.error("Moving file to temp folder due to error");
					logger.error("File not found");
					log.setStatus("FAILED");
					log.setJobEnded(new Date());
					logger.error(e.getMessage(), e);
				}
			} catch (Exception e) {
				logger.error("File not found");
				log.setStatus("FAILED");
				log.setJobEnded(new Date());
				logger.error(e.getMessage(), e);
			}
			jobLogRepository.save(log);
		}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/****
	 * @param filename
	 * 
	 *            Update the file name processed in DB
	 */
	private JobLog recordJobLog(String fileName, String status, String jobName) {
		JobLog log = new JobLog();
		log.setCreatedBy("Java Job Service");
		log.setDateCreated(new Date());
		log.setFileProcessed(fileName);
		log.setJobName(jobName);
		log.setJobStarted(new Date());
		log.setStatus(status);
		return jobLogRepository.save(log);

	}

	private String getTempPath() {
		List<Company> companys = companyRepository.findAll();
		return companys.get(0).getInterfaceTemp();
	}

	private String getAdHocPath() {
		List<Company> companys = companyRepository.findAll();
		return companys.get(0).getInterfaceAdhoc();
	}

	private void deleteFile(File file) {

		try {
			logger.info("deleting file " + file.getName());
			file.delete();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
