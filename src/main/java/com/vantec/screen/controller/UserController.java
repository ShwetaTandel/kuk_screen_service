package com.vantec.screen.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.vantec.screen.model.UserDTO;
import com.vantec.screen.service.UserService;

@RestController
@RequestMapping("/")
public class UserController {
	
	private static Logger logger = LoggerFactory.getLogger(UserController.class);
	
	
	@Autowired
	UserService userService;
	

	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	public ResponseEntity<Long> createUser(@RequestBody UserDTO userDTO){
		    
			try{
				logger.info("createUser called..");
				return new ResponseEntity<Long>(userService.createUser(userDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("UserDTO values : "    +   "FirstName : " + userDTO.getFirstName() +
		    			                                "LastName : "  + userDTO.getLastName() + 
		    			                                "UserLevel : " + userDTO.getUserLevel());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	
	@RequestMapping(value = "/editUser", method = RequestMethod.POST)
	public ResponseEntity<Long> editUser(@RequestBody UserDTO userDTO){
		    
			try{
				logger.info("EditUser called..");
				return new ResponseEntity<Long>(userService.editUser(userDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("UserDTO values : "    +   "FirstName : " + userDTO.getFirstName() +
		    			                                "LastName : "  + userDTO.getLastName() + 
		    			                                "UserLevel : " + userDTO.getUserLevel());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	
	@RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
	public ResponseEntity<Boolean> deleteUser(@RequestParam Long id){
		    
			try{
				logger.info("DeleteUser called..");
				return new ResponseEntity<Boolean>(userService.deleteUser(id), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Id values : "    +   "Id : " + id);
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	

	
}
