package com.vantec.screen.controller;


import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.screen.model.AisleDTO;
import com.vantec.screen.model.ColumnDTO;
import com.vantec.screen.model.InventoryDTO;
import com.vantec.screen.model.LocationDTO;
import com.vantec.screen.model.LocationFillingStatusDTO;
import com.vantec.screen.model.LocationTypeDTO;
import com.vantec.screen.model.ManifestDTO;
import com.vantec.screen.model.OdetteLblHistoryDTO;
import com.vantec.screen.model.PartDTO;
import com.vantec.screen.model.PickGroupDTO;
import com.vantec.screen.model.RackDTO;
import com.vantec.screen.model.ReasonCodeDTO;
import com.vantec.screen.model.RowDTO;
import com.vantec.screen.model.TrainDTO;
import com.vantec.screen.model.TrolleyDTO;
import com.vantec.screen.model.VendorDTO;
import com.vantec.screen.model.ZoneDestinationDTO;
import com.vantec.screen.model.ZoneTypeDTO;
import com.vantec.screen.service.ScreenService;

@RestController
@RequestMapping("/")
public class ScreenController {
	
	private static Logger logger = LoggerFactory.getLogger(ScreenController.class);
	
	
	@Autowired
	ScreenService screenService;
	

	@RequestMapping(value = "/createVendor", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createVendor(@RequestBody VendorDTO vendorDTO){
		    
			try{
				logger.info("createVendor called..");
				return new ResponseEntity<Boolean>(screenService.createVendor(vendorDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("VendorDTO values : "   +  "VendorName : " + vendorDTO.getVendorName() +
		    			                                "VendorCode : " + vendorDTO.getVendorCode() + 
		    			                                "CreatedBy : " + vendorDTO.getCreatedBy());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/editVendor", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editVendor(@RequestBody VendorDTO vendorDTO) {
		    
			
			try{
				logger.info("editVendor called..");
				return new ResponseEntity<Boolean>(screenService.editVendor(vendorDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("VendorDTO values : "   +  "VendorCode : " + vendorDTO.getVendorCode() +
		    			                                "VendorName : " + vendorDTO.getVendorName() + 
		    			                                "updatedBy : " + vendorDTO.getUpdatedBy());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	
	
	
	
	
	@RequestMapping(value = "/createTrain", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createTrain(@RequestBody TrainDTO trainDTO) {
			
			try{
				logger.info("createTrain called..");
				return new ResponseEntity<Boolean>(screenService.createTrain(trainDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("TRAINDTO values : " +  "Id : " + trainDTO.getId() +
		    			                                "Code : " + trainDTO.getTrainCode() + 
		    			                                "Description : " + trainDTO.getTrainDescription() + 
		    			                                "CreatedBy : " + trainDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + trainDTO.getUpdatedBy() ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
		
	@RequestMapping(value = "/editTrain", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editTrain(@RequestBody TrainDTO trainDTO) {
			
			try{
				logger.info("editTrain called..");
				return new ResponseEntity<Boolean>(screenService.editTrain(trainDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("TRAINDTO values : " +  "Id : " + trainDTO.getId() +
                        "Code : " + trainDTO.getTrainCode() + 
                        "Description : " + trainDTO.getTrainDescription() + 
                        "CreatedBy : " + trainDTO.getCreatedBy() + 
                        "UpdatedBy : " + trainDTO.getUpdatedBy() ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	@RequestMapping(value = "/deleteTrain", method = RequestMethod.GET)
	public ResponseEntity<Boolean> deleteTrain(@RequestParam Long trainId) {
			
			try{
				logger.info("deleteTrain called..");
				return new ResponseEntity<Boolean>(screenService.deleteTrain(trainId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : " +  " trainId : " + trainId ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	
	
	@RequestMapping(value = "/createTrolley", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createTrolley(@RequestBody TrolleyDTO trolleyDTO) {
			
			try{
				logger.info("createTrolley called..");
				return new ResponseEntity<Boolean>(screenService.createTrolley(trolleyDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("TROLLEY values : " +  "Id : " + trolleyDTO.getId() +
		    			                                "Code : " + trolleyDTO.getTrolleyCode() + 
		    			                                "Description : " + trolleyDTO.getTrolleyDescription() + 
		    			                                "CreatedBy : " + trolleyDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + trolleyDTO.getUpdatedBy() ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
		
	@RequestMapping(value = "/editTrolley", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editTrolley(@RequestBody TrolleyDTO trolleyDTO) {
			
			try{
				logger.info("editTrolley called..");
				return new ResponseEntity<Boolean>(screenService.editTrolley(trolleyDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("TROLLEY values : " +  "Id : " + trolleyDTO.getId() +
                        "Code : " + trolleyDTO.getTrolleyCode() + 
                        "Description : " + trolleyDTO.getTrolleyDescription() + 
                        "CreatedBy : " + trolleyDTO.getCreatedBy() + 
                        "UpdatedBy : " + trolleyDTO.getUpdatedBy() ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	@RequestMapping(value = "/deleteTrolley", method = RequestMethod.GET)
	public ResponseEntity<Boolean> deleteTrolley(@RequestParam Long trolleyId) {
			
			try{
				logger.info("deleteTrolley called..");
				return new ResponseEntity<Boolean>(screenService.deleteTrolley(trolleyId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : " +  " trolleyId : " + trolleyId ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	
	@RequestMapping(value = "/createZoneDestination", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createZoneDestination(@RequestBody ZoneDestinationDTO zoneDestDTO) {
			
			try{
				logger.info("createZoneDestination called..");
				return new ResponseEntity<Boolean>(screenService.createZoneDestination(zoneDestDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("values : " +  "Id : " + zoneDestDTO.getId() +
		    			                                "Code : " + zoneDestDTO.getZoneDestinationCode() + 
		    			                                "Description : " + zoneDestDTO.getZoneDestinationCode() + 
		    			                                "CreatedBy : " + zoneDestDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + zoneDestDTO.getUpdatedBy() ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
		
	@RequestMapping(value = "/editZoneDestination", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editZoneDestination(@RequestBody ZoneDestinationDTO zoneDestDTO) {
			
			try{
				logger.info("editZoneDestination called..");
				return new ResponseEntity<Boolean>(screenService.editZoneDestination(zoneDestDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("values : " +  "Id : " + zoneDestDTO.getId() +
		    			 "Code : " + zoneDestDTO.getZoneDestinationCode() + 
                         "Description : " + zoneDestDTO.getZoneDestinationCode() + 
                        "CreatedBy : " + zoneDestDTO.getCreatedBy() + 
                        "UpdatedBy : " + zoneDestDTO.getUpdatedBy() ); 
logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	@RequestMapping(value = "/deleteZoneDestination", method = RequestMethod.GET)
	public ResponseEntity<Boolean> deleteZoneDestination(@RequestParam Long zoneDestId) {
			
			try{
				logger.info("deleteZoneDestination called..");
				return new ResponseEntity<Boolean>(screenService.deleteZoneDestination(zoneDestId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : " +  " zonedetinationid : " + zoneDestId ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	
	
	@RequestMapping(value = "/createZoneType", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createZoneType(@RequestBody ZoneTypeDTO zoneTypeDTO) {
			
			try{
				logger.info("createZoneType called..");
				return new ResponseEntity<Boolean>(screenService.createZoneType(zoneTypeDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("ZoneTypeDTO values : " +  "Id : " + zoneTypeDTO.getId() +
		    			                                "ZoneTypeCode : " + zoneTypeDTO.getZoneTypeCode() + 
		    			                                "ZoneTypeDescription : " + zoneTypeDTO.getZoneTypeDescription() + 
		    			                                "CreatedBy : " + zoneTypeDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + zoneTypeDTO.getUpdatedBy() ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
		
	@RequestMapping(value = "/editZoneType", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editZoneType(@RequestBody ZoneTypeDTO zoneTypeDTO) {
			
			try{
				logger.info("editZoneType called..");
				return new ResponseEntity<Boolean>(screenService.editZoneType(zoneTypeDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("ZoneTypeDTO values : " +  "Id : " + zoneTypeDTO.getId() +
		    			                                "ZoneTypeCode : " + zoneTypeDTO.getZoneTypeCode() + 
		    			                                "ZoneTypeDescription : " + zoneTypeDTO.getZoneTypeDescription() + 
		    			                                "CreatedBy : " + zoneTypeDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + zoneTypeDTO.getUpdatedBy() ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	@RequestMapping(value = "/deleteZoneType", method = RequestMethod.GET)
	public ResponseEntity<Boolean> deleteZoneType(@RequestParam Long zoneTypeId) {
			
			try{
				logger.info("deleteZoneType called..");
				return new ResponseEntity<Boolean>(screenService.deleteZoneType(zoneTypeId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : " +  " zoneTypeId : " + zoneTypeId ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	
	
	
	@RequestMapping(value = "/createRow", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createRow(@RequestBody RowDTO rowDTO){
		    
			try{
				logger.info("createRow called..");
				return new ResponseEntity<Boolean>(screenService.createRow(rowDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("RowDTO values : "    +    "Id : " + rowDTO.getId() +
		    			                                "RowCode : " + rowDTO.getRowCode() + 
		    			                                "RowDescription : " + rowDTO.getRowDescription() + 
		    			                                "CreatedBy : " + rowDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + rowDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	@RequestMapping(value = "/editRow", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editRow(@RequestBody RowDTO rowDTO) {
			
			try{
				logger.info("editRow called..");
				return new ResponseEntity<Boolean>(screenService.editRow(rowDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("RowDTO values : "    +    "Id : " + rowDTO.getId() +
		    			                                "RowCode : " + rowDTO.getRowCode() + 
		    			                                "RowDescription : " + rowDTO.getRowDescription() + 
		    			                                "CreatedBy : " + rowDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + rowDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/deleteRow", method = RequestMethod.GET)
	public ResponseEntity<Boolean> deleteRow(@RequestParam Long rowId){
			
			try{
				logger.info("deleteRow called..");
				return new ResponseEntity<Boolean>(screenService.deleteRow(rowId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "    +    "rowId : " + rowId ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/createAisle", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createAisle(@RequestBody AisleDTO aisleDTO){
		    
			
			try{
				logger.info("createAisle called..");
				return new ResponseEntity<Boolean>(screenService.createAisle(aisleDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("AisleDTO values : "    +  "Id : " + aisleDTO.getId() +
		    			                                "AisleCode : " + aisleDTO.getAisleCode() + 
		    			                                "AisleDescription : " + aisleDTO.getAisleDescription() + 
		    			                                "CreatedBy : " + aisleDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + aisleDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/editAisle", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editAisle(@RequestBody AisleDTO aisleDTO){
		    
			
			try{
				logger.info("editAisle called..");
				return new ResponseEntity<Boolean>(screenService.editAisle(aisleDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("AisleDTO values : "    +  "Id : " + aisleDTO.getId() +
		    			                                "AisleCode : " + aisleDTO.getAisleCode() + 
		    			                                "AisleDescription : " + aisleDTO.getAisleDescription() + 
		    			                                "CreatedBy : " + aisleDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + aisleDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/deleteAisle", method = RequestMethod.GET)
	public ResponseEntity<Boolean> deleteAisle(@RequestParam Long aisleId) {
			
			try{
				logger.info("deleteAisle called..");
				return new ResponseEntity<Boolean>(screenService.deleteAisle(aisleId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("AisleDTO values : "    +  "aisleId : " + aisleId); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/createRack", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createRack(@RequestBody RackDTO rackDTO) {
			
			try{
				logger.info("createRack called..");
				return new ResponseEntity<Boolean>(screenService.createRack(rackDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("AisleDTO values : "    +  "Id : " + rackDTO.getId() +
		    			                                "RackCode : " + rackDTO.getRackCode() + 
		    			                                "RackDescription : " + rackDTO.getRackDescription() + 
		    			                                "CreatedBy : " + rackDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + rackDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/editRack", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editRack(@RequestBody RackDTO rackDTO){
		   
			
			try{
				 logger.info("editRack called..");
				 return new ResponseEntity<Boolean>(screenService.editRack(rackDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("AisleDTO values : "    +  "Id : " + rackDTO.getId() +
		    			                                "RackCode : " + rackDTO.getRackCode() + 
		    			                                "RackDescription : " + rackDTO.getRackDescription() + 
		    			                                "CreatedBy : " + rackDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + rackDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/deleteRack", method = RequestMethod.GET)
	public ResponseEntity<Boolean> deleteRack(@RequestParam Long rackId) {
			
			try{
				 logger.info("deleteRack called..");
				 return new ResponseEntity<Boolean>(screenService.deleteRack(rackId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "    +  "rackId : " + rackId); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/createColumn", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createColumn(@RequestBody ColumnDTO columnDTO){
		    
			
			try{
				logger.info("createColumn called..");
				return new ResponseEntity<Boolean>(screenService.createColumn(columnDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("ColumnDTO values : "   +  "Id : " + columnDTO.getId() +
		    			                                "ColumnCode : " + columnDTO.getColumnCode() + 
		    			                                "ColumnDescription : " + columnDTO.getColumnDescription() + 
		    			                                "CreatedBy : " + columnDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + columnDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/editColumn", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editColumn(@RequestBody ColumnDTO columnDTO){
			
			try{
				logger.info("editColumn called..");  
				return new ResponseEntity<Boolean>(screenService.editColumn(columnDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("ColumnDTO values : "   +  "Id : " + columnDTO.getId() +
		    			                                "ColumnCode : " + columnDTO.getColumnCode() + 
		    			                                "ColumnDescription : " + columnDTO.getColumnDescription() + 
		    			                                "CreatedBy : " + columnDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + columnDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/deleteColumn", method = RequestMethod.GET)
	public ResponseEntity<Boolean> deleteColumn(@RequestParam("columnId") Long columnId){
			
			try{
				logger.info("deleteColumn called.."); 
				return new ResponseEntity<Boolean>(screenService.deleteColumn(columnId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "columnId : " + columnId ); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/createLocationType", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createLocationType(@RequestBody LocationTypeDTO locationTypeDTO){
			
			try{
				logger.info("createLocationType called..");
				return new ResponseEntity<Boolean>(screenService.createLocationType(locationTypeDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("LocationTypeDTO values : "   +  "Id : " + locationTypeDTO.getId() +
		    			                                "LocationCode : " + locationTypeDTO.getLocationTypeCode() + 
		    			                                "LocationDescription : " + locationTypeDTO.getLocationTypeDescription() + 
		    			                                "CreatedBy : " + locationTypeDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + locationTypeDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/editLocationType", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editLocationType(@RequestBody LocationTypeDTO locationTypeDTO){
			
			try{
				logger.info("editLocationType called..");
				return new ResponseEntity<Boolean>(screenService.editLocationType(locationTypeDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("LocationTypeDTO values : "   +  "Id : " + locationTypeDTO.getId() +
		    			                                "LocationCode : " + locationTypeDTO.getLocationTypeCode() + 
		    			                                "LocationDescription : " + locationTypeDTO.getLocationTypeDescription() + 
		    			                                "CreatedBy : " + locationTypeDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + locationTypeDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/deleteLocationType", method = RequestMethod.GET)
	public ResponseEntity<Boolean> deleteLocationType(@RequestParam("locationTypeId") Long locationTypeId){
			
			try{
				logger.info("deleteLocationType called..");
				return new ResponseEntity<Boolean>(screenService.deleteLocationType(locationTypeId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("LocationTypeDTO values : "   +  "locationTypeId : " + locationTypeId); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/createPart", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createPart(@RequestBody PartDTO partDTO) throws ParseException {
		    
			try{
				logger.info("createPart called..");
				return new ResponseEntity<Boolean>(screenService.createPart(partDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("PartDTO values : "   +  "Id : " + partDTO.getId() +
		    			                                "PartNumber : " + partDTO.getPartNumber() + 
		    			                                "PartFamily : " + partDTO.getPartFamily() + 
		    			                                "CreatedBy : " + partDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + partDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/editPart", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editPart(@RequestBody PartDTO partDTO) throws ParseException {
		    
			try{
				logger.info("editPart called..");
				return new ResponseEntity<Boolean>(screenService.editPart(partDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("PartDTO values : "   +  "Id : " + partDTO.getId() +
		    			                                "PartNumber : " + partDTO.getPartNumber() + 
		    			                                "PartFamily : " + partDTO.getPartFamily() + 
		    			                                "CreatedBy : " + partDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + partDTO.getUpdatedBy()); 
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/createLocation", method = RequestMethod.POST)
	public ResponseEntity<List<String>> createLocation(@RequestBody LocationDTO locationDTO) {
		    
			try{
				logger.info("createLocation called..");
				return new ResponseEntity<List<String>>(screenService.createLocation(locationDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("LocationDTO values : "   +  "Id : " + locationDTO.getId() +
		    			                                "StartAisle : " + locationDTO.getStartAisle() + 
		    			                                "EndAisle : " + locationDTO.getEndAisle() + 
		    			                                "StartRack : " + locationDTO.getStartRack() + 
		    			                                "EndRack : " + locationDTO.getEndRack() + 
		    			                                "StartRow : " + locationDTO.getStartRow() + 
		    			                                "EndRow : " + locationDTO.getEndRow() + 
		    			                                "StartColumn : " + locationDTO.getStartColumn() + 
		    			                                "EndColumn : " + locationDTO.getEndColumn() +
		    			                                "CreatedBy : " + locationDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + locationDTO.getUpdatedBy() );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<List<String>>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/editLocation", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editLocation(@RequestBody LocationDTO locationDTO){
		    
			try{
				logger.info("editLocation called..");
				return new ResponseEntity<Boolean>(screenService.editLocation(locationDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("LocationDTO values : "   +  "Id : " + locationDTO.getId() +
		    			                                "StartAisle : " + locationDTO.getStartAisle() + 
		    			                                "EndAisle : " + locationDTO.getEndAisle() + 
		    			                                "StartRack : " + locationDTO.getStartRack() + 
		    			                                "EndRack : " + locationDTO.getEndRack() + 
		    			                                "StartRow : " + locationDTO.getStartRow() + 
		    			                                "EndRow : " + locationDTO.getEndRow() + 
		    			                                "StartColumn : " + locationDTO.getStartColumn() + 
		    			                                "EndColumn : " + locationDTO.getEndColumn() +
		    			                                "CreatedBy : " + locationDTO.getCreatedBy() + 
		    			                                "UpdatedBy : " + locationDTO.getUpdatedBy() );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	@RequestMapping(value = "/editInventory", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editInventory(@RequestBody InventoryDTO inventoryDTO){
		    
			try{
				logger.info("editInventory called..");
				return new ResponseEntity<Boolean>(screenService.editInventory(inventoryDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("InventoryDTO values : "  +"PartNumber : " + inventoryDTO.getPartNumber() +
		    			                                "SerialNumber : " + inventoryDTO.getSerialNumber() + 
		    			                                "StatusCode : " + inventoryDTO.getStatusCode() + 
		    			                                "CurrentUser : " + inventoryDTO.getCurrentUser() + 
		    			                                "Decant : " + inventoryDTO.getDecant() + 
		    			                                "Inspect : " + inventoryDTO.getInspect());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/editManifest", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editManifest(@RequestBody ManifestDTO manifestDTO){
		    
			try{
				logger.info("editManifest called..");
				return new ResponseEntity<Boolean>(screenService.editManifest(manifestDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("ManifestDTO values : "+  "DocumentReference : " + manifestDTO.getDocumentReference() + 
								                       "ExpectedDeliverTime : " + manifestDTO.getExpectedDeliverTime() + 
								                       "UpdatedBy : " + manifestDTO.getUpdatedBy());
	    			                                
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	

	
	@RequestMapping(value = "/createReasonCode", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createReasonCode(@RequestBody ReasonCodeDTO reasonCodeDTO){
		
			try{
				logger.info("createReasonCode called..");
				return new ResponseEntity<Boolean>(screenService.createReasonCode(reasonCodeDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("ReasonCodeDTO values : "+       "DocDetailId : " + reasonCodeDTO.getReasonCode() + 
								                              "TransactedQty : " + reasonCodeDTO.getReasonDescription() + 
								                              "UpdatedBy : " + reasonCodeDTO.getUserId());
	    			                                
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	@RequestMapping(value = "/editReasonCode", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editReasonCode(@RequestBody ReasonCodeDTO reasonCodeDTO) {
		    
			try{
				logger.info("editReasonCode called..");
				return new ResponseEntity<Boolean>(screenService.editReasonCode(reasonCodeDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("ReasonCodeDTO values : "+       "DocDetailId : " + reasonCodeDTO.getReasonCode() + 
								                              "TransactedQty : " + reasonCodeDTO.getReasonDescription() + 
								                              "UpdatedBy : " + reasonCodeDTO.getUserId());
	    			                                
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/createLocationFillingStatus", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createLocationFillingStatus(@RequestBody LocationFillingStatusDTO locationFillingStatusDTO){
		   
			try{
				 logger.info("createLocationFillingStatus called..");
				 return new ResponseEntity<Boolean>(screenService.createLocationFillingStatus(locationFillingStatusDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("LocationFillingStatusDTO values : "+       "FillingCode : " + locationFillingStatusDTO.getFillingCode() + 
								                              "FillingStatusId : " + locationFillingStatusDTO.getFillingStatusId() + 
								                              "UserId : " + locationFillingStatusDTO.getUserId());
	    			                                
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/editLocationFillingStatus", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editLocationFillingStatus(@RequestBody LocationFillingStatusDTO locationFillingStatusDTO) {
		    
			try{
				logger.info("editLocationFillingStatus called..");
				return new ResponseEntity<Boolean>(screenService.editLocationFillingStatus(locationFillingStatusDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("LocationFillingStatusDTO values : "+       "FillingCode : " + locationFillingStatusDTO.getFillingCode() + 
								                              "FillingStatusId : " + locationFillingStatusDTO.getFillingStatusId() + 
								                              "UserId : " + locationFillingStatusDTO.getUserId());
	    			                                
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/createPickGroup", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createPickGroup(@RequestBody PickGroupDTO pickGroupDTO){
		    
			 try{
				    logger.info("createPickGroup called..");
					return new ResponseEntity<Boolean>(screenService.createPickGroup(pickGroupDTO), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("PickGroupDTO values : " + "id : " + pickGroupDTO.getId() +
			    			                                "pickGroupCode : " + pickGroupDTO.getPickGroupCode() + 
			    			                                "pickGroupDescription : " + pickGroupDTO.getPickGroupDescription() + 
			    			                                "pallet : " + pickGroupDTO.getPallet() + 
			    			                                "maxCaseQty : " + pickGroupDTO.getMaxCaseQty() + 
			    			                                "userId : " + pickGroupDTO.getUserId() + 
			    			                                "altPickGroup : " + pickGroupDTO.getAltPickGroup());
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
			 
	}
	
	@RequestMapping(value = "/editPickGroup", method = RequestMethod.POST)
	public ResponseEntity<Boolean> editPickGroup(@RequestBody PickGroupDTO pickGroupDTO){
		    try{
		    	logger.info("editPickGroup called..");
		    	return new ResponseEntity<Boolean>(screenService.editPickGroup(pickGroupDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("PickGroupDTO values : " + "id : " + pickGroupDTO.getId() +
		    			                                "pickGroupCode : " + pickGroupDTO.getPickGroupCode() + 
		    			                                "pickGroupDescription : " + pickGroupDTO.getPickGroupDescription() + 
		    			                                "pallet : " + pickGroupDTO.getPallet() + 
		    			                                "maxCaseQty : " + pickGroupDTO.getMaxCaseQty() + 
		    			                                "userId : " + pickGroupDTO.getUserId() + 
		    			                                "altPickGroup : " + pickGroupDTO.getAltPickGroup());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/deleteScanData", method = RequestMethod.GET)
	public ResponseEntity<Boolean> deleteScanData(@RequestParam("id") Long id){
		    try{
		    	logger.info("deleteScanData called..");
		    	return new ResponseEntity<Boolean>(screenService.deleteScanData(id), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("deleteScanData values : " + "id : " + id);
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ResponseEntity<String> test(){
		    try{
		    	logger.info("test called..");
		    	return new ResponseEntity<String>("Well done, you have reached the service", HttpStatus.OK);
		    }catch(Exception ex){
		    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	@RequestMapping(value = "/createOdetteLblHistory", method = RequestMethod.POST)
	public ResponseEntity<Boolean> createOdetteLblHistory(@RequestBody OdetteLblHistoryDTO odetteLblHistoryDTO){
		    try{
		    	logger.info("createOdetteLblHistory called..");
		    	return new ResponseEntity<Boolean>(screenService.createOdetteLblHistory(odetteLblHistoryDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("createOrder values : " + "ran: " + odetteLblHistoryDTO.getRanOrder()  +
		    	                                       "part : " + odetteLblHistoryDTO.getPartNumber() + 
		    	                                       "snp : " + odetteLblHistoryDTO.getSnp());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
}
