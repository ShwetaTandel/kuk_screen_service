package com.vantec.screen.controller;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.screen.entity.Company;
import com.vantec.screen.entity.JobLog;
import com.vantec.screen.repository.CompanyRepository;
import com.vantec.screen.repository.JobLogRepository;
import com.vantec.screen.service.JobService;

@RestController
@RequestMapping("/")
public class JobController {

	private static Logger logger = LoggerFactory.getLogger(JobController.class);

	@Autowired
	JobService jobService;

	@Autowired
	JobLogRepository jobLogRepository;

	@Autowired
	CompanyRepository companyRepository;

	@RequestMapping(value = "/processDailyFile", method = RequestMethod.GET)
	public ResponseEntity<Boolean> processDailyFile(@RequestParam String fileName) {
		String inpPath = getAdHocPath() + "\\" + fileName;
		File file = new File(inpPath);
		if (file.exists()) {
			logger.info("Reading file " + file.getName());

			JobLog log = recordJobLog(fileName, "PROCESSING", "Daily " + fileName + " File");
			try {

				if (fileName.equalsIgnoreCase("PICKPLAN")) {
					jobService.readPickPlanFile(file);
				} else if (fileName.equalsIgnoreCase("RECEIPTPLAN")) {
					jobService.readReceiptPlanFile(file);
				} else if (fileName.equalsIgnoreCase("parts")) {
					jobService.readDailyPartFile(file);
				}else if (fileName.startsWith("recon")) {
					jobService.readReconFile(file);
				}

				deleteFile(file);
				log.setStatus("SUCCESS");
				log.setJobEnded(new Date());
			} catch (Exception e) {
				logger.error("Exception is " + e.getMessage() + "- Cause:" + e.getCause());
				logger.error(e.getMessage(), e);
				log.setStatus("FAILED");
				log.setJobEnded(new Date());
				jobLogRepository.save(log);
				return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			jobLogRepository.save(log);
		}
		return new ResponseEntity<Boolean>(HttpStatus.OK);

	}

	/**
	 * 
	 * @param file
	 */
	private void deleteFile(File file) {

		try {
			logger.info("deleting file " + file.getName());
			file.delete();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	/****
	 * @param filename
	 * 
	 *            Update the file name processed in DB
	 */
	private JobLog recordJobLog(String fileName, String status, String jobName) {
		JobLog log = new JobLog();
		log.setCreatedBy("Java Job Service");
		log.setDateCreated(new Date());
		log.setFileProcessed(fileName);
		log.setJobName(jobName);
		log.setJobStarted(new Date());
		log.setStatus(status);
		return jobLogRepository.save(log);

	}

	/**
	 * get AdHoc PathO
	 * @return
	 */
	private String getAdHocPath() {
		List<Company> companys = companyRepository.findAll();
		return companys.get(0).getInterfaceAdhoc();
	}

}
