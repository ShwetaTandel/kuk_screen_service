package com.vantec.screen.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.screen.model.SaDTO;
import com.vantec.screen.model.SplitPieceDTO;
import com.vantec.screen.service.TransactionService;

@RestController
@RequestMapping("/")
public class TransactionController {
	
	private static Logger logger = LoggerFactory.getLogger(TransactionController.class);
	
	
	@Autowired
	TransactionService transactionService;
	

	@RequestMapping(value = "/saOut", method = RequestMethod.POST)
	public ResponseEntity<Boolean> saOut(@RequestBody SaDTO saOutDTO){
			
			try{
		    	logger.info("saOut called..");
		    	return new ResponseEntity<Boolean>(transactionService.saOut(saOutDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("SaDTO values : " + "PartNumber : " + saOutDTO.getPartNumber() +
    			                                 "SerialRReference : " + saOutDTO.getSerialReference()+ 
    			                                 "Qty : " + saOutDTO.getQty() + 
    			                                 "CreatedBy : " + saOutDTO.getCreatedBy() + 
    			                                 "FromLocationCode : " + saOutDTO.getFromLocationCode());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	
	@RequestMapping(value = "/saIn", method = RequestMethod.POST)
	public ResponseEntity<Boolean> saIn(@RequestBody SaDTO saInDTO){
			
			try{
		    	logger.info("saIn called.. " + saInDTO.getPartNumber() +"  "+ saInDTO.getSerialReference());
		    	return new ResponseEntity<Boolean>(transactionService.saIn(saInDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("SaDTO values : " + "PartNumber : " + saInDTO.getPartNumber() +
    			                                 "SerialRReference : " + saInDTO.getSerialReference()+ 
    			                                 "Qty : " + saInDTO.getQty() + 
    			                                 "CreatedBy : " + saInDTO.getCreatedBy() + 
    			                                 "ToLocationCode : " + saInDTO.getToLocationCode());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
			
	}
	
	@RequestMapping(value = "/saInValidation", method = RequestMethod.GET)
	public ResponseEntity<String> saInValidation(@RequestParam String serialReference,@RequestParam String partNumber,@RequestParam String locationCode) {
			
			try{
		    	logger.info("saInValidation called..");
		    	return new ResponseEntity<String>(transactionService.saInValidation(partNumber,locationCode,serialReference), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : " + "PartNumber : " + partNumber +
    			                           "SerialRReference : " + serialReference+ 
    			                           "LocationCode : " + locationCode );
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	
	@RequestMapping(value = "/splitPieceValidation", method = RequestMethod.GET)
	public ResponseEntity<String> splitPieceValidation(@RequestParam String serialReference,@RequestParam String partNumber) {
			
			try{
		    	logger.info("splitPieceValidation called..");
		    	return new ResponseEntity<String>(transactionService.splitPieceValidation(partNumber,serialReference), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : " + "PartNumber : " + partNumber +
    			                           "SerialRReference : " + serialReference);
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	
	@RequestMapping(value = "/splitPiece", method = RequestMethod.POST)
	public ResponseEntity<Boolean> splitPiece(@RequestBody SplitPieceDTO splitPieceDTO) {
			
			try{
		    	logger.info("splitPieceValidation called..");
		    	
		    	SaDTO saOutDTO = new SaDTO();
		    	saOutDTO.setSerialReference(splitPieceDTO.getSerial1());
		    	saOutDTO.setPartNumber(splitPieceDTO.getPartNumber());
		    	saOutDTO.setQty(splitPieceDTO.getQty());
		    	saOutDTO.setCreatedBy(splitPieceDTO.getCreatedBy());
		    	saOutDTO.setReasonCode("QA PIECEOUT");
				//SaOUT
		    	logger.info("Sa Out ..");
				transactionService.saOut(saOutDTO);
				
				//SaIN
				SaDTO saInDTO = new SaDTO();
				saInDTO.setSerialReference(splitPieceDTO.getSerial2());
				saInDTO.setPartNumber(splitPieceDTO.getPartNumber());
				saInDTO.setQty(splitPieceDTO.getQty());
				saInDTO.setCreatedBy(splitPieceDTO.getCreatedBy());
				saInDTO.setReasonCode("SPLIT SERIAL");
				saInDTO.setToLocationCode(splitPieceDTO.getToLocationCode());
				saInDTO.setSendMsg(false);;
				logger.info("Sa In ..");
				return new ResponseEntity<Boolean>(transactionService.saIn(saInDTO), HttpStatus.OK);
				
		    }catch(Exception ex){
		    	logger.error("Values : " + "PartNumber : " + splitPieceDTO.getPartNumber() +
		    			                    "ToLocationCode : " + splitPieceDTO.getToLocationCode() +
		    			                   "Qty : " + splitPieceDTO.getQty() +
    			                           "SerialRReference : " + splitPieceDTO.getSerial1());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	}
	
	
}
