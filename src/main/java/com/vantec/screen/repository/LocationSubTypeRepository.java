package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.LocationSubType;



public interface LocationSubTypeRepository extends JpaRepository<LocationSubType, Serializable>{
	
	LocationSubType findById(Long id);
	LocationSubType findByLocationSubTypeCode(String locationSubTypeCode);
}
