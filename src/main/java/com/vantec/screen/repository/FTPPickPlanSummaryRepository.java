package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vantec.screen.entity.FtpPickplanSummary;

public interface FTPPickPlanSummaryRepository extends JpaRepository<FtpPickplanSummary, Serializable>{
	
	@Query(value="select * from ftp_pickplan_summary where external_order not in (select PP1ORNO from ftp_pickplan)",nativeQuery = true)
	List<FtpPickplanSummary> findRecordsNotInPickPLan();
	
	FtpPickplanSummary findByExternalOrder(String externalOrder);
}

