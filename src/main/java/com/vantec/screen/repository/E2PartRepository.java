package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vantec.screen.entity.E2Part;

public interface E2PartRepository extends JpaRepository<E2Part, Serializable> {
	
	@Query("select a from E2Part a where not exists (select partNumber from Part b where a.partNumber = b.partNumber)")
	List<E2Part> notExistsInPartTable();
	
	@Query("select a from E2Part a where exists (select partNumber from Part b where a.partNumber = b.partNumber)")
	List<E2Part> existsInPartTable();
	
	@Query("select a from E2Part a where a.supplier in (select vendorReferenceCode from Vendor) and not exists (select partNumber from Part b where a.partNumber = b.partNumber )")
	List<E2Part> notExistsInPartTableCheckVendor();
	
	@Query("select a from E2Part a where exists (select partNumber from Part b where a.partNumber = b.partNumber and a.supplier in (select vendorName from Vendor))")
	List<E2Part> existsInPartTableCheckVendor();
	
}

