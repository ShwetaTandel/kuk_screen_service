package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.JobDefinition;

public interface JobDefinitionRepository extends JpaRepository<JobDefinition, Serializable>{
	
	JobDefinition findByJobName(String jobName);
}

