package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.Vendor;

public interface VendorRepository extends JpaRepository<Vendor, Serializable>{

	Vendor findByVendorReferenceCode(String vendorCode);
	
	Vendor findById(Long id);

}
