package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.PackType;

public interface PackTypeRepository extends JpaRepository<PackType, Serializable>{

	PackType findById(Long id);

}
