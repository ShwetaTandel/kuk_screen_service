package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.screen.entity.InventoryMaster;
import com.vantec.screen.entity.InventoryStatus;
import com.vantec.screen.entity.Location;
import com.vantec.screen.entity.Part;

public interface InventoryMasterRepository extends JpaRepository<InventoryMaster, Serializable>{

	List<InventoryMaster> findByLocationCode(String locationCode);
	
	List<InventoryMaster> findByPart(Part part);
	
	@Query(value="SELECT SUM(CASE WHEN inventory_status_code = 'QTINE' or  inventory_status_code = 'OVERBOOKING' THEN inventory_qty  ELSE 0 END) AS holdStatusQty, sum(inventory_qty) as invQty,"
	+" sum(hold_qty) as holdQty  FROM kuk.inventory_master, inventory_status  where inventory_master.inventory_status_id = inventory_status.id and part_number =  :partNumber",nativeQuery = true)
	List<Object[]> findInventoryByPartNumber(@Param("partNumber") String partNumber);
	
	InventoryMaster findBySerialReferenceAndPartNumber(String serialReference,String partNumber );
	
	List<InventoryMaster> findByTagReferenceLike(String tagReference);

	List<InventoryMaster> findBylocationCodeAndInventoryStatusNotIn(String locationCode, InventoryStatus status);

	List<InventoryMaster> findByCurrentLocationAndInventoryStatus(Location location, InventoryStatus status);
	
	List<InventoryMaster> findByLocationCodeIn(List<Location> locations);
}

