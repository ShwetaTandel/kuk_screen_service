package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.LocationRow;

public interface LocationRowRepository extends JpaRepository<LocationRow, Serializable>{

	LocationRow findById(Long id);
	
	LocationRow findByRowCode(String rowCode);
}

