package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.E2Recon;

public interface E2ReconRepository extends JpaRepository<E2Recon, Serializable> {
	
	
}

