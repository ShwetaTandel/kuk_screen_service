package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.LocationColumn;

public interface LocationColumnRepository extends JpaRepository<LocationColumn, Serializable>{

	LocationColumn findById(Long id);
	
	LocationColumn findByColumnCode(String columnCode);
}

