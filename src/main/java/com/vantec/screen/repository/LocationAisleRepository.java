package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.LocationAisle;

public interface LocationAisleRepository extends JpaRepository<LocationAisle, Serializable>{

	LocationAisle findById(Long id);
	
	LocationAisle findByAisleCode(String aisleCode);
}

