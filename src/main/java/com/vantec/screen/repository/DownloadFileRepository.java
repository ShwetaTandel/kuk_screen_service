package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.DownloadFile;

public interface DownloadFileRepository extends JpaRepository<DownloadFile, Serializable>{
}
