package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.ZoneDestination;

public interface ZoneDestinationRepository extends JpaRepository<ZoneDestination, Serializable>{
	ZoneDestination findById(Long id);	
}

