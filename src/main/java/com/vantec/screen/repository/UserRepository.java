package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.User;

public interface UserRepository extends JpaRepository<User, Serializable>{

	User findById(Long id);

	

	
}

