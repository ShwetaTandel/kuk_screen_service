package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.LocationType;

public interface LocationTypeRepository extends JpaRepository<LocationType, Serializable>{

	LocationType findById(Long id);
	
	LocationType findByLocationTypeCode(String code);
	
	List<LocationType>  findByLocationArea(String area);
}

