package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.InventoryMaster;
import com.vantec.screen.entity.PickBody;
import com.vantec.screen.entity.PickDetail;

public interface PickDetailRepository extends JpaRepository<PickDetail, Serializable>{

	List<PickDetail> findByInventoryMaster(InventoryMaster inventoryMasterId);
	
	List<PickDetail> findByPickBody(PickBody pickBody);
	
	List<PickDetail> findByDocumentReferenceOrderByLineNoAsc(String documentReference);
}

