package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.JobLog;

public interface JobLogRepository extends JpaRepository<JobLog, Serializable>{
	
	JobLog findById(Long id);
}

