package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.UploadFile;

public interface UploadFileRepository extends JpaRepository<UploadFile, Serializable>{
	


}
