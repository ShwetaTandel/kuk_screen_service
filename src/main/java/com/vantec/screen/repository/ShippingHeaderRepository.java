package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.ShippingHeader;

public interface ShippingHeaderRepository extends JpaRepository<ShippingHeader, Serializable>{

	ShippingHeader findByDocumentReference(String documentReference);
}

