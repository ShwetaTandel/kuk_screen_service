package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.screen.entity.TransactionHistory;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, Serializable>{
	
//	/List<TransactionHistory> findByShortCode(String shortCode);
	
	@Query("select t from TransactionHistory t where t.shortCode = :shortCode and t.dateUpdated > :dateUpdated order by dateCreated asc")
    List<TransactionHistory> findAllWithShortCodeAndTimeAfter(@Param("shortCode") String shortCode,@Param("dateUpdated") Date dateUpdated);
	
	@Query("select t from TransactionHistory t where t.shortCode = :shortCode and t.toLocationCode in ('MISS02','MISS03') and t.dateUpdated > :dateUpdated order by dateCreated asc")
    List<TransactionHistory> findAllWithShortCodeToLocationCodeAndTimeAfter(@Param("shortCode") String shortCode,@Param("dateUpdated") Date dateUpdated);
	
	@Query("select t from TransactionHistory t where t.shortCode in ('PIM','LPI') and t.dateUpdated > :dateUpdated order by dateUpdated asc")
    List<TransactionHistory> findAllPIMLPIShortCodeAndTimeAfter(@Param("dateUpdated") Date dateUpdated);
	
}

