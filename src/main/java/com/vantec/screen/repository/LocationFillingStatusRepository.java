package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.LocationFillingStatus;

public interface LocationFillingStatusRepository extends JpaRepository<LocationFillingStatus, Serializable>{

	LocationFillingStatus findById(Long id);
	
	LocationFillingStatus findByFillingCode(String fillingCode);
}

