package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.Train;

public interface TrainRepository extends JpaRepository<Train, Serializable>{
	Train findById(Long id);	

}

