package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vantec.screen.entity.FtpPickplan;

public interface FTPPickPlanRepository extends JpaRepository<FtpPickplan, Serializable>{
	// PP1STS as PP1STS,    PP1PSTMP as PP1PSTMP,    PP1PONO as PP1PONO,    PP1TYP as PP1TYP,    PP1PNO as PP1PNO,    "
		//	+"PP1PCQ as PP1PCQ,    PP1OPER as PP1OPER,    PP1ILOC as PP1ILOC,    PP1ITM as PP1ITM,    PP1BSTMP as PP1BSTMP,    PP1SSTMP as PP1SSTMP,    PP1DSTR as PP1DSTR,"
		//	+"PP1DNM as PP1DNM,    PP1PROJ as PP1PROJ,    PP1TTYPE as PP1TTYPE,    PP1PARENT as PP1PARENT,    PP1PROJECT as PP1PROJECT
	
	//@Query("SELECT *, max(pp1oper), max(pp1parent), max(pp1project), max(pp1itm) FROM kuk.ftp_pickplan group by PP1ORNO")
	@Query(value="SELECT     PP1ORNO as PP1ORNO, PP1STS as PP1STS,PP1TYP as PP1TYP, PP1PARENT as PP1PARENT,  PP1PSTMP as PP1PSTMP, PP1BSTMP as PP1BSTMP,  PP1SSTMP as PP1SSTMP, PP1DSTR as PP1DSTR,PP1DNM as PP1DNM,"
	+"max(pp1oper) as maxpp1oper, max(pp1parent) as maxpp1parent, max(pp1project) as maxpp1project, max(pp1itm) as maxppitm FROM ftp_pickplan group by PP1ORNO",nativeQuery = true)
	 List<Object[]> findAllGroupBYOrderNo();
}

