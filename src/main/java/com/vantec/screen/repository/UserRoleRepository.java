package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Serializable>{

	List<UserRole> findAllByUserId(Long id);
	
}

