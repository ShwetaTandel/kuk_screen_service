package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Serializable>{

	List<Company> findAll();
}
