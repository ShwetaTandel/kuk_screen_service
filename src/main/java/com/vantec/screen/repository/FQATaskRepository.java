package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.FqaTask;

public interface FQATaskRepository extends JpaRepository<FqaTask, Serializable>{

	FqaTask findById(Long id);
}

