package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.screen.entity.ReceiptBody;
import com.vantec.screen.entity.ReceiptDetail;

public interface ReceiptDetailRepository extends JpaRepository<ReceiptDetail, Serializable>{

	ReceiptDetail findByDocumentReferenceAndPartNumberAndSerialReference(String documentReference,String partNumber,String serialReference);
	
	ReceiptDetail findBySerialReference(String serialReference);
	
	List<ReceiptDetail> findByDocumentReference(String documentReference);
	
	List<ReceiptDetail> findByDocumentReferenceAndPartNumber(String documentReference,String partNumber);
	
	ReceiptDetail findBySerialReferenceAndPartNumberAndReceiptBody(String serialReference,String partNumber,ReceiptBody receiptBody);


}
