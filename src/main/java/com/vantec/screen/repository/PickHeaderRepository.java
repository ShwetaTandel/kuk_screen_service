package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.PickHeader;

public interface PickHeaderRepository extends JpaRepository<PickHeader, Serializable>{
	
}

