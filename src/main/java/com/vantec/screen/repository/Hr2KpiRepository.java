package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.Hr2Kpi;

public interface Hr2KpiRepository extends JpaRepository<Hr2Kpi, Serializable>{
}

