package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.PickGroup;

public interface PickGroupRepository extends JpaRepository<PickGroup, Serializable>{

	PickGroup findById(Long id);
}