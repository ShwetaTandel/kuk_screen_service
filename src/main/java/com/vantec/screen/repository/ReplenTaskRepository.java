package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.ReplenTask;

public interface ReplenTaskRepository extends JpaRepository<ReplenTask, Serializable>{
	
	ReplenTask findById(Long id);
	
	List<ReplenTask> findByPickGroupIdAndProcessedAndPalletReferenceIsNullOrderByPriorityAscDateCreatedAsc(Long pickGroupId,Boolean processed);
	
	List<ReplenTask> findByPalletReference(String tagReference);
	
	ReplenTask findBySuggestedSerialAndPartIdAndProcessed(String serial,Long partId,Boolean processed);
	
	ReplenTask findByScannedSerialAndPartIdAndProcessed(String serial,Long partId,Boolean processed);
	
	List<ReplenTask> findByInventoryMasterId(Long inventoryMasterId);
	
	List<ReplenTask> findByPickGroupIdAndProcessed(Long pickGroupId,Boolean processed);
	
}

