package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.Location;
import com.vantec.screen.entity.LocationType;

public interface LocationRepository extends JpaRepository<Location, Serializable>{

	Location findById(Long id);
	
	Location findByLocationCode(String locationCode);
	
	List<Location> findByLocationTypeIn(List<LocationType> locationTypes);
}

