package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.DocumentStatus;
import com.vantec.screen.entity.ReceiptHeader;

public interface ReceiptHeaderRepository extends JpaRepository<ReceiptHeader, Serializable>{

	ReceiptHeader findByDocumentReference(String documentReference);
	
	ReceiptHeader findByCustomerReference(String documentReference);
	
	List<ReceiptHeader> findByHaulierReference(String haulierReference);
	
	List<ReceiptHeader> findByHaulierReferenceAndDocumentStatus(String haulierReference,DocumentStatus status);
	
	ReceiptHeader findByDeliveryNoteAndHaulierReference(String deliveryNote,String haulierReference);


}
