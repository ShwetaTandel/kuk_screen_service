package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.InventoryStatus;

public interface InventoryStatusRepository extends JpaRepository<InventoryStatus, Serializable>{

	InventoryStatus findById(Long id);
	
	InventoryStatus findByInventoryStatusCode(String statusCode);
}

