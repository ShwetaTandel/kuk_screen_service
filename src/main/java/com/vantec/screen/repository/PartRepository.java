package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vantec.screen.entity.Part;

public interface PartRepository extends JpaRepository<Part, Serializable>{

	Part findById(Long id);
	
	Part findByPartNumber(String partNumber);
	
	List<Part> findAllByOrderByPartNumber();
	
	@Query("select a from Part a where exists (select partNumber from E2Part b where a.partNumber = b.partNumber)")
	List<Part> existsInE2PartTable();

}

