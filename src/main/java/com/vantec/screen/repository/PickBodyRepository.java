package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.PickBody;
import com.vantec.screen.entity.PickHeader;

public interface PickBodyRepository extends JpaRepository<PickBody, Serializable>{
	
	List<PickBody> findByPickHeader(PickHeader pickHeader);
}

