package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.ReasonCode;

public interface ReasonCodeRepository extends JpaRepository<ReasonCode, Serializable>{

	ReasonCode findById(Long id);

}

