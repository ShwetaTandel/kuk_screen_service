package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.DocumentStatus;

public interface DocumentStatusRepository extends JpaRepository<DocumentStatus, Serializable>{
	
	DocumentStatus findByDocumentStatusCode(String statusCode);
	
}

