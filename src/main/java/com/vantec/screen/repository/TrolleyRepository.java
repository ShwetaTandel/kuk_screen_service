package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.Trolley;

public interface TrolleyRepository extends JpaRepository<Trolley, Serializable>{
	Trolley findById(Long id);	

}

