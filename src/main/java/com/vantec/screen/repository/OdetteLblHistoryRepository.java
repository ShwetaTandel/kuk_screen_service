package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.OdetteLblHistory;

public interface OdetteLblHistoryRepository extends JpaRepository<OdetteLblHistory, Serializable>{

	
}

