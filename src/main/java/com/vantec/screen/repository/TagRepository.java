package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.Location;
import com.vantec.screen.entity.Tag;

public interface TagRepository extends JpaRepository<Tag, Serializable>{

	List<Tag> findByCurrentLocation(Location location);
}

