package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.ReceiptBody;
import com.vantec.screen.entity.ReceiptHeader;

public interface ReceiptBodyRepository extends JpaRepository<ReceiptBody, Serializable>{

	List<ReceiptBody> findByDocumentReferenceAndPartNumber(String documentReference,String partNumber);

	ReceiptBody findByPartNumber(String partNumber);
	
	List<ReceiptBody> findByDocumentReference(String documentReference);
	
	List<ReceiptBody> findByPartNumberAndReceiptHeader(String partNumber,ReceiptHeader receiptHeader);
	
	ReceiptBody findById(Long id);


}
