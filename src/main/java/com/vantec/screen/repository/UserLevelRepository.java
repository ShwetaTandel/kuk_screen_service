package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.UserLevel;

public interface UserLevelRepository extends JpaRepository<UserLevel, Serializable>{

	UserLevel findByUserLevel(String userLevel);
}

