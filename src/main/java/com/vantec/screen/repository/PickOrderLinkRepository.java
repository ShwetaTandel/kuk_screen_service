package com.vantec.screen.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.screen.entity.OrderBody;
import com.vantec.screen.entity.PickDetail;
import com.vantec.screen.entity.PickOrderLink;

public interface PickOrderLinkRepository extends JpaRepository<PickOrderLink, Serializable>{
	
	List<PickOrderLink> findByPickDetail(PickDetail detail);
	
}

