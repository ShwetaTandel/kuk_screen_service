package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.LocationRack;

public interface LocationRackRepository extends JpaRepository<LocationRack, Serializable>{

	LocationRack findById(Long id);
	
	LocationRack findByRackCode(String rackCode);
}

