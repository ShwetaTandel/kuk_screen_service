package com.vantec.screen.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.screen.entity.ScanData;

public interface ScanDataRepository extends JpaRepository<ScanData, Serializable>{
	
	ScanData findById(Long id);

}
