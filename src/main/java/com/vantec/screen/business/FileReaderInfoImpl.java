package com.vantec.screen.business;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.vantec.screen.entity.E2Part;

@Service("fileReaderInfo")
public class FileReaderInfoImpl implements FileReaderInfo{
	private static Logger LOGGER = LoggerFactory.getLogger(FileReaderInfoImpl.class);
	private String[] splitLine(String line) {
		// Split Line at |
		line = line.replaceAll("\"", "");
		line = line.concat("|End");
		return line.split("\\|");
	}

//	public List<E2Part> readDailyPartFile(File file) throws IOException {
//
//		// Sample Lines
//		// 01010D1025|BOLT (DLF)|.0800|N|0|4|.0270|7318158898|3|Purchased|PRD000266
//		// PNO|Locn Description |Item Cost|DDC Part Flag|DDC Offset|Count Freq|Weight|Commodity|Safety Time|Purch.Text|Supplier
//		FileReader fileReader = new FileReader(file);
//		BufferedReader lines = new BufferedReader(fileReader);
//		String line = "";
//		List<String> partNumbers = new ArrayList<String>();
//
//		List<E2Part> partDTOs = new ArrayList<E2Part>();
//		while ((line = lines.readLine()) != null) {
//
//			String[] values = splitLine(line);
//			E2Part e2part = new E2Part();
//			e2part.setPartNumber(values[0]);
//			e2part.setLocationDesc(values[1]);
//			e2part.setItemCost(Double.valueOf(values[2]));
//			e2part.setDdcPartFlag(values[3]);
//			e2part.setDdcOffset(values[4]);
//			e2part.setCountFreq(values[5]);
//			e2part.setWeight(Double.valueOf(values[6]));
//			e2part.setCommodity(values[7]);
//			e2part.setSafetyTime(Integer.valueOf(values[8]));
//			e2part.setPurchaseText(values[9]);
//			e2part.setSupplier(values[10]);
//			e2part.setDateCreated(new Date());
//			//Don't add duplicate parts
//			if(!partNumbers.contains(values[0])){
//				partDTOs.add(e2part);
//				partNumbers.add(values[0]);
//			}else{
//				LOGGER.info("Duplicate part ignored.."+values[0] + " ... for vendor "+values[10]);
//			}
//		}
//
//		fileReader.close();
//		lines.close();
//
//		return partDTOs;
//
//	}
	public List<E2Part> readDailyPartFile(File file) throws IOException {

		// Sample Lines
		// 01010D1025|BOLT (DLF)|.0800|N|0|4|.0270|7318158898|3|Purchased|PRD000266
		// PNO|Locn Description |Item Cost|DDC Part Flag|DDC Offset|Count Freq|Weight|Commodity|Safety Time|Purch.Text|Supplier
		FileInputStream inputStream = null;
		Scanner sc = null;
		List<String> partNumbers = new ArrayList<String>();

		List<E2Part> partDTOs = new ArrayList<E2Part>();
		try {
		    inputStream = new FileInputStream(file);
		    sc = new Scanner(inputStream, "UTF-8");
		    while (sc.hasNextLine()) {
		        String line = sc.nextLine();
		        String[] values = splitLine(line);
				E2Part e2part = new E2Part();
				e2part.setPartNumber(values[0]);
				e2part.setLocationDesc(values[1]);
				e2part.setItemCost(Double.valueOf(values[2]));
				e2part.setDdcPartFlag(values[3]);
				e2part.setDdcOffset(values[4]);
				e2part.setCountFreq(values[5]);
				e2part.setWeight(Double.valueOf(values[6]));
				e2part.setCommodity(values[7]);
				e2part.setSafetyTime(Integer.valueOf(values[8]));
				e2part.setPurchaseText(values[9]);
				e2part.setSupplier(values[10]);
				e2part.setDateCreated(new Date());
				//Don't add duplicate parts
				if(!partNumbers.contains(values[0])){
					partDTOs.add(e2part);
					partNumbers.add(values[0]);
				}else{
					LOGGER.info("Duplicate part ignored.."+values[0] + " ... for vendor "+values[10]);
				}
		    }
		    // note that Scanner suppresses exceptions
		    if (sc.ioException() != null) {
		        throw sc.ioException();
		    }
		} finally {
			partNumbers.clear();
		    if (inputStream != null) {
		        inputStream.close();
		    }
		    if (sc != null) {
		        sc.close();
		    }
		}
		return partDTOs;

	}
}
