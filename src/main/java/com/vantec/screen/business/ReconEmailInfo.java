package com.vantec.screen.business;

import java.io.IOException;
import java.util.List;

import com.vantec.screen.entity.E2Recon;

public interface ReconEmailInfo {
	
	 public  String writeReconFile(List<E2Recon> result) throws IOException;
	 
	
}
