package com.vantec.screen.business;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.vantec.screen.entity.E2Part;

public interface FileReaderInfo {
	
	public List<E2Part> readDailyPartFile(File file) throws IOException;
	
	 
	 
	
}
