package com.vantec.screen.business;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.vantec.screen.entity.E2Recon;

@Service("reconEmailInfo")
public class ReconEmailInfoImpl implements ReconEmailInfo{
	
	
	public static String path="C:\\reports\\email\\";
	
	 
	 public String writeReconFile(List<E2Recon> result) throws IOException{
		 
	        XSSFWorkbook workbook = new XSSFWorkbook();
	        XSSFSheet sheet = workbook.createSheet("FRECONC");
	        
	        int rowCount = 0;
	        Row row = sheet.createRow(rowCount);
	        Cell cell = row.createCell(1);
           cell.setCellValue("PARTNO");
           cell = row.createCell(2);
           cell.setCellValue("BAAN_INV");
           cell = row.createCell(3);
           cell.setCellValue("VIMS_INV");
           cell = row.createCell(4);
           cell.setCellValue("VIMS_HOLD");
           cell = row.createCell(5);
           cell.setCellValue("DIFF");
	       
	        for(E2Recon recon : result){
	        	int columnCount = 0;
	        	row = sheet.createRow(++rowCount);
	            cell = row.createCell(++columnCount);
	            cell.setCellValue(recon.getPartNumber());
	            cell = row.createCell(++columnCount);
	            cell.setCellValue(recon.getKukQty());
	            cell = row.createCell(++columnCount);
	            cell.setCellValue(recon.getVimsQty());
	            cell = row.createCell(++columnCount);
	            cell.setCellValue(recon.getVimsHoldQty());
	            cell = row.createCell(++columnCount);
	            cell.setCellValue(recon.getDiffQty());
	            
	            
	        }
	        
	        String filename = "RECONSNAP"+new SimpleDateFormat("ddMMyyyy").format(new Date())+".xlsx";  
	        FileOutputStream outputStream = new FileOutputStream(path+filename);  
	        workbook.write(outputStream);
	        workbook.close();
	        return filename;

		 
	 }

}
