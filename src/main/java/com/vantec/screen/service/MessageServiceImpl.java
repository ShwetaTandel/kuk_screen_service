package com.vantec.screen.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.screen.entity.Company;
import com.vantec.screen.entity.UploadFile;
import com.vantec.screen.repository.CompanyRepository;
import com.vantec.screen.repository.DocumentStatusRepository;
import com.vantec.screen.repository.PickBodyRepository;
import com.vantec.screen.repository.PickDetailRepository;
import com.vantec.screen.repository.PickHeaderRepository;
import com.vantec.screen.repository.ReceiptBodyRepository;
import com.vantec.screen.repository.ReceiptDetailRepository;
import com.vantec.screen.repository.ReceiptHeaderRepository;
import com.vantec.screen.repository.UploadFileRepository;
import com.vantec.screen.util.ConstantHelper;



@Service("messageService")
public class MessageServiceImpl implements MessageService{
	
	private static Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);
	@Autowired
	ReceiptHeaderRepository receiptHeaderRepository;
	
	@Autowired
	ReceiptBodyRepository receiptBodyRepository;
	
	@Autowired
	ReceiptDetailRepository receiptDetailRepository;
	

	
	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	DocumentStatusRepository documentStatusRepository;
	
	@Autowired
	PickHeaderRepository pickHeaderRepository;
	
	@Autowired
	PickBodyRepository pickBodyRepository;
	
	@Autowired
	PickDetailRepository pickDetailRepository;

	@Autowired
	UploadFileRepository uploadFileRepository;
	
	
	public Boolean writeSaInRTSMessage(String partNumber, Double qty, String user, String reasonCode) throws IOException{
		Company company = companyRepository.findAll().get(0);


		logger.info(	"SA IN ->"+partNumber+ " QTY ->"+qty);
		if (partNumber != null) {
			String filetypename =ConstantHelper.RETURN_FILE_TYPE_UNPLANNED_RTS;
			UploadFile savedFile = createFileRecord(user, company, filetypename );
			logger.info("File records create for "+savedFile.getFilename() + " ->" + savedFile.getType() + "->"+savedFile.getId());
			String filename = filetypename + String.format("%09d", savedFile.getId());

			Path tempPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TMP_FILE_EXT);
			Path textPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TEXT_FILE_EXT);

			//UNPLANNED ------------>
			//WA	UNRTS	2079797961	8
			//WA	RTSCC	2070372651	8
			//OORG	ORNO	PNO	QTY
			//Origin	External Order	Part No	Quantity
			//Origin	Fixed set to 'WA'			
			//External Order	"UNRTS for Unplanned RTS,
			//RTSCC for Unplanned RTS with Reason code 18"			
			//Part	WMS Part Number			
			//Qty	Quantity			



			BufferedWriter writer = Files.newBufferedWriter(tempPath);
			writer.write(ConstantHelper.BAAN_FILE_START_FILE_RECORD);
			writer.write("\n");
			int lineCnt = 0;
			String exOrder =  ConstantHelper.UNPLANNED_RTS_FILE_RETURN_TYPE_GENERAL;
			if (reasonCode.equalsIgnoreCase(ConstantHelper.UNPLANNED_REASON_CODE_RTS_PROJ)) {
				exOrder = ConstantHelper.UNPLANNED_RTS_FILE_RETURN_TYPE_FOR_REASON_CODE_18;
			}
			writer.write(ConstantHelper.UNPLANNED_RTS_FILE_RETURN_ORIGIN +"|" +exOrder + "|" + partNumber + "|-" + qty.intValue());
			lineCnt++;

			writer.write("\n");
			writer.write(ConstantHelper.BAAN_FILE_END_FILE_RECORD+" - " + lineCnt + " record written");
			// Copy data from temp to txt file and delete temp file
			writer.close();
			Files.copy(tempPath, textPath, StandardCopyOption.REPLACE_EXISTING);
			Files.delete(tempPath);


			savedFile.setFilename(filename + ".txt");
			uploadFileRepository.save(savedFile);
			return true;
		}
		return false;

	
		
	}
	private UploadFile createFileRecord(String userId, Company company,String type) {

		UploadFile receiptMessageFile = new UploadFile();
		receiptMessageFile.setFilename(type);
		
		receiptMessageFile.setType(type);
		receiptMessageFile.setFullPath(company.getInterfaceOutput());
		receiptMessageFile.setUpdatedBy(userId);
		receiptMessageFile.setDateCreated(new Date());
		receiptMessageFile.setLastUpdated(new Date());
		UploadFile savedFile = uploadFileRepository.save(receiptMessageFile);
		return savedFile;

	}
	
	
	public String getLineNo(int line,int maxvalue){
		
		String lineNumber = "";
		int numberOfDigits = 0;
		numberOfDigits = String.valueOf(line).length();
		for (int i = 0; i < maxvalue - numberOfDigits; i++) {
			lineNumber += "0";
		}
		lineNumber = lineNumber + String.valueOf(line);
		return lineNumber;
	}
	
	public static void main(String args[]){
	}

     
	

	
	private String getReceiptQuantityFormat(Double qty){
		String pattern = ".000";
		DecimalFormat decimalFormat = new DecimalFormat(pattern);

		String number = decimalFormat.format(qty/1000);
		return number;
	}
	
	private String getqty(Integer a){
		
		Double b = a.doubleValue()/1000;
		String c =  b.toString();
		
		
		String[] d = c.split("\\.");
		
		if(Integer.parseInt(d[1])==0){
			return(d[0]);
		}else{
			return(c);
		}
	}
	
	
	
	
	




	
 
}
