package com.vantec.screen.service;

import java.io.IOException;
import java.util.Date;

import com.vantec.screen.entity.Company;

public interface ReportService {
	
	
	 public String generateParts(Company company, Date prevTime,String  currentTime) throws IOException;
	 
	 public String generateInventorys(Company company, Date prevTime,String  currentTime) throws IOException;
	 
	 public String generateHistory(Company company, Date prevTime,String  currentTime) throws IOException;

	
	 public String generateF6Data(Company company, Date prevTime,String  currentTime) throws IOException ;
	
	
	 public String generatePim(Company company, Date prevTime,String  currentTime) throws IOException;

}
