package com.vantec.screen.service;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.screen.business.FileReaderInfo;
import com.vantec.screen.dao.PartsDAO;
import com.vantec.screen.entity.E2Part;
import com.vantec.screen.entity.E2Recon;
import com.vantec.screen.entity.FtpPickplan;
import com.vantec.screen.entity.FtpPickplanSummary;
import com.vantec.screen.entity.FtpReceiptplan;
import com.vantec.screen.entity.Part;
import com.vantec.screen.entity.Vendor;
import com.vantec.screen.repository.CompanyRepository;
import com.vantec.screen.repository.DocumentStatusRepository;
import com.vantec.screen.repository.DownloadFileRepository;
import com.vantec.screen.repository.E2PartRepository;
import com.vantec.screen.repository.E2ReconRepository;
import com.vantec.screen.repository.FTPPickPlanRepository;
import com.vantec.screen.repository.FTPPickPlanSummaryRepository;
import com.vantec.screen.repository.FTPReceiptPlanRepository;
import com.vantec.screen.repository.InventoryMasterRepository;
import com.vantec.screen.repository.JobLogRepository;
import com.vantec.screen.repository.LocationSubTypeRepository;
import com.vantec.screen.repository.LocationTypeRepository;
import com.vantec.screen.repository.PartRepository;
import com.vantec.screen.repository.TransactionHistoryRepository;
import com.vantec.screen.repository.VendorRepository;
import com.vantec.screen.util.ConstantHelper;
import com.vantec.screen.util.JobFileHandlerHelper;

@Service("jobService")
public class JobServiceImpl implements JobService {

	@Autowired
	PartRepository partMasterRepository;

	@Autowired
	E2PartRepository e2PartRepository;

	@Autowired
	TransactionHistoryRepository transactionHistoryRepository;

	@Autowired
	DocumentStatusRepository documentStatusRepository;
	
	@Autowired
	InventoryMasterRepository inventoryMasterRepository;

	@Autowired
	VendorRepository vendorRepo;

	@Autowired
	DownloadFileRepository downloadFileRepo;
	
	@Autowired
	JobLogRepository jobLogRepository;

	@Autowired
	private PartsDAO partsDao;

	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	LocationTypeRepository locTypeRepo;

	@Autowired
	LocationSubTypeRepository locSubTypeRepo;

	@Autowired
	FTPReceiptPlanRepository ftpReceiptRepo;

	@Autowired
	FTPPickPlanRepository ftpPickPlanRepo;
	
	@Autowired
	E2ReconRepository e2ReconRepository;
	
	//@Autowired
	//FTPPickPlanDTORepository ftpPickPlanDTORepo;
	
	@Autowired
	FTPPickPlanSummaryRepository ftpPickPlanSummaryRepo;
	
	@Autowired
	private Environment environment;
	
	@Autowired
	private FileReaderInfo fileReaderInfo;

	private static Logger LOGGER = LoggerFactory.getLogger(JobServiceImpl.class);

	@Override
	@Transactional
	public void readDailyPartFile(File file) throws IOException {
		List<E2Part> e2parts = null;
		List<E2Part> e2PartsToAdd  = null;
		try {
			LOGGER.info("Deleting records..");
			e2PartRepository.deleteAll();
			LOGGER.info("Reading records from file..");
			// recordFileRead(file.getName(), "Daily Part File");

			// List<E2Part> e2parts =
			// JobFileHandlerHelper.readDailyPartFile(file);
			e2parts = fileReaderInfo.readDailyPartFile(file);
			LOGGER.info("Inserting into E2Part..");
			e2PartRepository.save(e2parts);
			if (environment.acceptsProfiles("prod")) {
				LOGGER.info("IN prod environment");
				e2PartsToAdd = e2PartRepository.notExistsInPartTableCheckVendor();
				LOGGER.info("parts to add ----" + e2PartsToAdd.size());
				partMasterRepository.save(populateParts(e2PartsToAdd));
				// update the parts in existing queries
				LOGGER.info("Updating the existing parts ...");
				partsDao.updatePartsWithDescription();
				LOGGER.info("Updating the existing parts finished...");
			} else {
				e2PartsToAdd = e2PartRepository.notExistsInPartTable();
				LOGGER.info("parts to add ----" + e2PartsToAdd.size());
				partMasterRepository.save(populateParts(e2PartsToAdd));
				// update the parts in existing queries
				LOGGER.info("Updating the existing parts ...");
				partsDao.updatePartsWithDescription();
				LOGGER.info("Updating the existing parts finished...");
			}
		} finally {
			
			e2parts= null;
			e2PartsToAdd = null;
			System.gc();
		}

	}

	@Transactional
	public void processPickPlanSummary() {

		LOGGER.info("Deleting records..which are not n pick Plan");
		List<FtpPickplanSummary> ids = ftpPickPlanSummaryRepo.findRecordsNotInPickPLan();
		ftpPickPlanSummaryRepo.delete(ids);
		LOGGER.info("Get all orders from pick plan");
		List<Object[]> records = ftpPickPlanRepo.findAllGroupBYOrderNo();
		List<FtpPickplanSummary> summList = new ArrayList<FtpPickplanSummary>();
		for (Object[] pickPlan : records) {
			summList.add(addUpdatePickPlanSummaryRecord(pickPlan));
			//System.out.println(pickPlan[0]);
		}
		ftpPickPlanSummaryRepo.save(summList);
		LOGGER.info("Pick Summary updated successfully");
	}	
	
	@Transactional
	public void readReceiptPlanFile(File file) throws IOException {
		LOGGER.info("Deleting records..");
		ftpReceiptRepo.deleteAll();
		LOGGER.info("Reading records from file ReceiptPlan..");
		//recordFileRead(file.getName(), "Daily ReceiptPlan file");
		List<FtpReceiptplan> records = JobFileHandlerHelper.readReceiptPlan(file);
		LOGGER.info("Inserting into FtpReceiptplan ..");
		ftpReceiptRepo.save(records);

	}

	@Transactional
	public void readPickPlanFile(File file) throws IOException {
		LOGGER.info("Deleting records..");
		ftpPickPlanRepo.deleteAllInBatch();
		LOGGER.info("Reading records from file ftpPickPlan..");
		//recordFileRead(file.getName(), "Daily PickPlan file");
		List<FtpPickplan> records = JobFileHandlerHelper.readFtpPickPlan(file);
		LOGGER.info("Inserting into ftpPickPlan ..");
		ftpPickPlanRepo.save(records);
		
		LOGGER.info("Pick Plan Job Successful");
		LOGGER.info("Calling Pick plan summary ");
		processPickPlanSummary();
		
		

	}
	
	public void readReconFile(File file) throws IOException {
		LOGGER.info("Deleting records..");
		e2ReconRepository.deleteAll();
		LOGGER.info("Reading records from file recon file..");
		List<E2Recon> records = JobFileHandlerHelper.readReconFile(file);
		LOGGER.info("Inserting into e2recon ..");
		for(E2Recon recon : records){
			Part part = partMasterRepository.findByPartNumber(recon.getPartNumber());
			if(part!=null){
				recon.setPartId(part.getId());
				List<Object[]> invs  = inventoryMasterRepository.findInventoryByPartNumber(recon.getPartNumber());
				for (Object[] invRecord : invs) {
					double holdQty =0, holdStatusQty=0, invQty=0;
					if (invRecord[0] != null) {
						holdStatusQty = Double.valueOf(invRecord[0].toString());
					}
					if (invRecord[1] != null) {
						invQty = Double.valueOf(invRecord[1].toString());
					}
					if (invRecord[2] != null) {
						holdQty = Double.valueOf(invRecord[2].toString());
					}
					
					recon.setVimsHoldQty(holdQty + holdStatusQty);
					recon.setVimsQty(invQty);
				}
				recon.setDiffQty(recon.getKukQty() - recon.getVimsQty());
				e2ReconRepository.save(recon);
			}
			
		}
		
	}

	

	

	/**
	 * Convert E2Part to Part
	 * 
	 * @param e2parts
	 * @return
	 */
	private List<Part> populateParts(List<E2Part> e2parts) {

		List<Part> newPartsToAdd = new ArrayList<Part>();
		Vendor vendor = vendorRepo.findByVendorReferenceCode(ConstantHelper.VENDOR_KUK_REF_CODE);
		for (E2Part e2part : e2parts) {

			Part part = new Part();
			part.setPartNumber(e2part.getPartNumber());
			part.setVendor(vendor);
			try {
				part.setEffectiveFrom(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2000-01-01 00:00:00"));
				part.setEffectiveTo(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2035-12-31 00:00:00"));
			} catch (ParseException e) {
			}
			part.setVersion(0L);
			part.setActive(true);
			part.setImaginaryPart(false);
			part.setRequiresCount(false);
			part.setRequiresDecant(false);
			part.setRequiresInspection(false);
			part.setSafetyStock(0);
			part.setFullBoxPick(false);
			part.setModular(false);
			part.setFixedLocationType(locTypeRepo.findByLocationTypeCode(ConstantHelper.LOCATION_NEW));
			part.setFixedSubType(locSubTypeRepo.findByLocationSubTypeCode(ConstantHelper.LOCATION_NEW));
			part.setIncludeInventory(0);
			part.setLoosePartQty(0);
			part.setConversionFactor(1);
			part.setPartDescription(e2part.getLocationDesc());
			part.setWeight(e2part.getWeight());
			part.setCreatedBy(ConstantHelper.DAILYJOBUSER);
			part.setLastUpdatedBy(ConstantHelper.DAILYJOBUSER);
			part.setDateCreated(new Date());
			part.setLastUpdated(new Date());
			part.setIsAS400(false);
			newPartsToAdd.add(part);
		}

		return newPartsToAdd;

	}
	
	/**
	 * Method to add and update the pick plan summary based on pick plan record
	 * @param pickPlan
	 */
	private FtpPickplanSummary addUpdatePickPlanSummaryRecord(Object[] pickPlan) {
		FtpPickplanSummary summ = ftpPickPlanSummaryRepo.findByExternalOrder(pickPlan[0].toString());
		try {
			
			boolean isUpdate = true;
			boolean isDataChanged = false;
			if (summ == null) {
				isUpdate = false;
				summ = new FtpPickplanSummary();
				summ.setDateCreated(new Date());
			}
			if (isUpdate == false) {
				summ.setExternalOrder(pickPlan[0].toString());
				if (pickPlan[1] != null) {
					summ.setPstatus(pickPlan[1].toString());
				}
				if (pickPlan[2] != null) {
					summ.setTransType(pickPlan[2].toString());
				}

				Object parent = pickPlan[3];
				if (parent != null) {
					summ.setParentPart(parent.toString().substring(0, parent.toString().indexOf('-')));
				}

			}
			if (pickPlan[4] != null && !pickPlan[4].toString().trim().isEmpty()) {
				String prodStmp = pickPlan[4].toString();
				String year = "20" + prodStmp.substring(6, 8);
				String month = prodStmp.substring(3, 5);
				String date = prodStmp.substring(0, 2);
				String time = prodStmp.substring(9, 11) + prodStmp.substring(12, 14);
				if (isUpdate == false || summ.getProductionDate() == null || (isUpdate && summ.getProductionDate().intValue() != Integer.valueOf(year + month + date))) {
					isDataChanged = true;
					summ.setProductionDate(Integer.valueOf(year + month + date));
					summ.setProductionTime(Integer.valueOf(time));
				}

			}

			if (pickPlan[5] != null && !pickPlan[5].toString().trim().isEmpty()) {
				String buildStmp = pickPlan[5].toString();
				String year = "20" + buildStmp.substring(6, 8);
				String month = buildStmp.substring(3, 5);
				String date = buildStmp.substring(0, 2);
				String time = buildStmp.substring(9, 11) + buildStmp.substring(12, 14);
				if (isUpdate == false || summ.getBuildDate() == null || (isUpdate && summ.getBuildDate().intValue() != Integer.valueOf(year + month + date))) {
					isDataChanged = true;
					summ.setBuildDate(Integer.valueOf(year + month + date));
					summ.setBuildTime(Integer.valueOf(time));
				}

			}

			if (pickPlan[6] != null && !pickPlan[6].toString().trim().isEmpty()) {
				String salesStmp = pickPlan[6].toString();
				String year = "20" + salesStmp.substring(6, 8);
				String month = salesStmp.substring(3, 5);
				String date = salesStmp.substring(0, 2);
				String time = salesStmp.substring(9, 11) + salesStmp.substring(12, 14);
				if (isUpdate == false || summ.getSalesDate() ==null || (isUpdate && summ.getSalesDate().intValue() != Integer.valueOf(year + month + date))) {
					isDataChanged = true;
					summ.setSalesDate(Integer.valueOf(year + month + date));
					summ.setSalesTime(Integer.valueOf(time));
				}

			}
			if (isUpdate == false || (summ.getOperation() != null && pickPlan[9] != null
					&& pickPlan[9].toString().compareTo(summ.getOperation()) > 0)) {
				isDataChanged = true;
				if (pickPlan[9] != null) {
					summ.setOperation(pickPlan[9].toString());
				}

			}
			if (isUpdate == false || (summ.getParentPart() != null && pickPlan[10] != null
					&& pickPlan[10].toString().compareTo(summ.getParentPart()) > 0)) {
				isDataChanged = true;
				if (pickPlan[10] != null) {
					summ.setParentPart(pickPlan[10].toString());
				}

			}
			if (isUpdate == false || (summ.getProject() != null && pickPlan[11] != null
					&& pickPlan[11].toString().compareTo(summ.getProject()) > 0)) {
				isDataChanged = true;
				if (pickPlan[11] != null) {
					summ.setProject(pickPlan[11].toString());
				}
				if (pickPlan[7] != null) {
					summ.setDistributor(pickPlan[7].toString());
				}
				if (pickPlan[8] != null) {
					summ.setDistributorName(pickPlan[8].toString());
				}

			}
			if (isUpdate == false || (summ.getItem() != null && pickPlan[12] != null
					&& pickPlan[12].toString().compareTo(summ.getItem()) > 0)) {
				isDataChanged = true;
				if (pickPlan[12] != null) {
					summ.setItem(pickPlan[12].toString());
				}
			}

			if (isDataChanged) {
				summ.setDateUpdated(new Date());
			}
		
		} catch (Exception e) {
				LOGGER.info("Exception occured in while adding/updation ="+pickPlan[0].toString());
				LOGGER.info("Values are  ="+Arrays.toString(pickPlan));
		}
		return (summ);
	}

}
