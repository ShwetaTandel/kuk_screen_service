package com.vantec.screen.service;

import java.io.File;
import java.io.IOException;

public interface JobService {
	
	
	 public void readDailyPartFile(File file) throws IOException;
	 public void readReceiptPlanFile(File file) throws IOException;
	 public void readPickPlanFile(File file) throws IOException;
	 public void processPickPlanSummary();
	 public void readReconFile(File file) throws IOException;
	 

}
