package com.vantec.screen.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.screen.command.CustomValueGeneratorCommand;
import com.vantec.screen.command.UserNameGenerator;
import com.vantec.screen.entity.User;
import com.vantec.screen.entity.UserRole;
import com.vantec.screen.model.UserDTO;
import com.vantec.screen.repository.UserLevelRepository;
import com.vantec.screen.repository.UserRepository;
import com.vantec.screen.repository.UserRoleRepository;



@Service("userService")
public class UserServiceImpl implements UserService{
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserLevelRepository userLevelRepository;
	
	@Autowired
	UserRoleRepository userRoleRepository;
	

	@Override
	public Long createUser(UserDTO userDTO) {
		
		User user = new User();
		user.setUsername(UUID.randomUUID().toString());
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setUserLevel(userLevelRepository.findByUserLevel(userDTO.getUserLevel()));
		user.setPassword(userDTO.getPassword());
		user.setEnabled(true);
		user = userRepository.save(user);
		
		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new UserNameGenerator();
		String userReference = customValueGeneratorCommand.generate(user.getId(), "RRMC");
		user.setUsername(userReference);
		userRepository.save(user);
		return user.getId();
	}

	@Override
	public Long editUser(UserDTO userDTO) {
		
		User user = userRepository.findById(userDTO.getId());
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setUserLevel(userLevelRepository.findByUserLevel(userDTO.getUserLevel()));
		userRepository.save(user);
		return user.getId();
	}

	@Override
	public Boolean deleteUser(Long id) {
		
		
		
		List<UserRole> userRole = userRoleRepository.findAllByUserId(id);
		userRoleRepository.deleteInBatch(userRole);
		
		User user = userRepository.findById(id);
		userRepository.delete(user);;
		return true;
		
	}


}
