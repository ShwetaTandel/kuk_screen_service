package com.vantec.screen.service;

import java.io.IOException;

public interface MessageService {
	
	public Boolean writeSaInRTSMessage(String partNumber, Double qty, String user, String reasonCode) throws IOException;
	
}
