package com.vantec.screen.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.screen.entity.Hr2Kpi;
import com.vantec.screen.entity.InventoryMaster;
import com.vantec.screen.entity.Location;
import com.vantec.screen.entity.LocationType;
import com.vantec.screen.entity.PickGroup;
import com.vantec.screen.entity.ReplenTask;
import com.vantec.screen.repository.Hr2KpiRepository;
import com.vantec.screen.repository.InventoryMasterRepository;
import com.vantec.screen.repository.LocationRepository;
import com.vantec.screen.repository.LocationTypeRepository;
import com.vantec.screen.repository.PickGroupRepository;
import com.vantec.screen.repository.ReplenTaskRepository;



@Service("hr2KpiService")
public class Hr2KpiServiceImpl implements Hr2KpiService{
	
	@Autowired
	InventoryMasterRepository inventoryMasterRepository;
	
	@Autowired
	Hr2KpiRepository hr2KpiRepository;
	
	@Autowired
	PickGroupRepository pickGroupRepository;
	
	@Autowired
	ReplenTaskRepository replenTaskRepository;
	
	@Autowired
	LocationTypeRepository locationTypeRepository;
	
	@Autowired
	LocationRepository locationRepository;

	@Override
	public void updateKPILocationCount() {
		
		
		String[] locationArray = {"DOCK1","DOCK2","DOCK3","DOCK4","DOCK5","DOCK6","DOCK7","DOCK8","DOCKJ","PARCEL","PARC1",
				                  "PARC2","PARC3","PARC4","DOCKPI","RELOCDOCK","EMRG",
				                  "Miss01","Miss02","Miss03",
				                  "MAR1","MAR2","MAR3"};
		
		
		
		for(int i=0;i<locationArray.length ;i++){
			List<InventoryMaster> dock1Count =  inventoryMasterRepository.findByLocationCode(locationArray[i]);
			
			Hr2Kpi hr2Kpi = new Hr2Kpi();
			hr2Kpi.setCount(dock1Count.size());
			hr2Kpi.setLocationCode(locationArray[i]);
			hr2Kpi.setDateCreated(new Date());
			hr2KpiRepository.save(hr2Kpi);
		}
		
		
		
	}
	
	public static void main(String args[]){
		String[] locationArray = {"DOCK1","DOCK2","DOCK3","DOCK4","DOCK5","DOCK6","DOCK7","DOCK8","DOCKJ","PARCEL","PARC1",
                "PARC2","PARC3","PARC4","DOCKPI","RELOCDOCK","EMRG"};
		
		
		for(int i=0;i<locationArray.length ;i++){
			
		}
	}

	@Override
	public void updateKPIreplenCount() {
		
		List<PickGroup> replenGroups = pickGroupRepository.findAll();
		
		for(PickGroup replenGroup:replenGroups){
			
			List<ReplenTask> replen =  replenTaskRepository.findByPickGroupIdAndProcessed(replenGroup.getId(), false);
			
			Hr2Kpi hr2Kpi = new Hr2Kpi();
			hr2Kpi.setCount(replen.size());
			hr2Kpi.setReplenCode(replenGroup.getPickGroupCode());
			hr2Kpi.setDateCreated(new Date());
			hr2KpiRepository.save(hr2Kpi);
			
		}
		
	}

	@Override
	public void updatePickLocationCount() {
		
		List<LocationType> locationTypes = locationTypeRepository.findByLocationArea("storage");
		List<Location> locations = locationRepository.findByLocationTypeIn(locationTypes);
		List<InventoryMaster> inventorys =  inventoryMasterRepository.findByLocationCodeIn(locations);
		
		Hr2Kpi hr2Kpi = new Hr2Kpi();
		hr2Kpi.setCount(inventorys.size());
		hr2Kpi.setLocationCode("PICK");
		hr2Kpi.setDateCreated(new Date());
		hr2KpiRepository.save(hr2Kpi);
		
	}
	
	

}
