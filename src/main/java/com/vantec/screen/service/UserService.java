package com.vantec.screen.service;

import com.vantec.screen.model.UserDTO;

public interface UserService {

	Long createUser(UserDTO userDTO);

	Long editUser(UserDTO userDTO);

	Boolean deleteUser(Long id);




}
