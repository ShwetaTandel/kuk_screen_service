package com.vantec.screen.service;

import com.vantec.screen.model.SaDTO;

public interface TransactionService {

	Boolean saOut(SaDTO saOutDTO);

	Boolean saIn(SaDTO saInDTO);

	String saInValidation(String serialReference,String partNumber, String locationCode);
	
	 public Boolean recordTransaction(SaDTO saDTO);

	String splitPieceValidation(String partNumber, String serialReference);

}
