package com.vantec.screen.service;

import java.text.ParseException;
import java.util.List;

import com.vantec.screen.model.AisleDTO;
import com.vantec.screen.model.ColumnDTO;
import com.vantec.screen.model.InventoryDTO;
import com.vantec.screen.model.LocationDTO;
import com.vantec.screen.model.LocationFillingStatusDTO;
import com.vantec.screen.model.LocationTypeDTO;
import com.vantec.screen.model.ManifestDTO;
import com.vantec.screen.model.OdetteLblHistoryDTO;
import com.vantec.screen.model.PartDTO;
import com.vantec.screen.model.PickGroupDTO;
import com.vantec.screen.model.RackDTO;
import com.vantec.screen.model.ReasonCodeDTO;
import com.vantec.screen.model.RowDTO;
import com.vantec.screen.model.TrainDTO;
import com.vantec.screen.model.TrolleyDTO;
import com.vantec.screen.model.VendorDTO;
import com.vantec.screen.model.ZoneDestinationDTO;
import com.vantec.screen.model.ZoneTypeDTO;

public interface ScreenService {


	Boolean createVendor(VendorDTO vendorDTO) throws ParseException;

	Boolean editVendor(VendorDTO vendorDTO) throws ParseException;

//	Boolean createCharge(ChargeDTO chargeDTO);
//
//	Boolean editCharge(ChargeDTO chargeDTO);

	Boolean createZoneType(ZoneTypeDTO zoneTypeDTO);

	Boolean editZoneType(ZoneTypeDTO zoneTypeDTO);

	Boolean createRow(RowDTO rowDTO);

	Boolean editRow(RowDTO rowDTO);

	Boolean createAisle(AisleDTO aisleDTO);

	Boolean editAisle(AisleDTO aisleDTO);

	Boolean createColumn(ColumnDTO columnDTO);

	Boolean editColumn(ColumnDTO columnDTO);

	Boolean createLocationType(LocationTypeDTO locationTypeDTO);

	Boolean editLocationType(LocationTypeDTO locationTypeDTO);

	Boolean createPart(PartDTO partDTO) throws ParseException;

	Boolean editPart(PartDTO partDTO) throws ParseException;

//	Boolean createChargeType(ChargeTypeDTO chargeTypeDTO);
//
//	Boolean editChargeType(ChargeTypeDTO chargeTypeDTO);

	Boolean createRack(RackDTO rackDTO);

	Boolean editRack(RackDTO rackDTO);
	
	Boolean deleteRack(Long rackId);

	Boolean deleteRow(Long rowId);

	Boolean deleteAisle(Long aisleId);
	

	
	Boolean deleteColumn(Long rackId);
	
	Boolean deleteLocationType(Long locationTypeId);

	Boolean deleteZoneType(Long zoneTypeId);

	List<String> createLocation(LocationDTO locationDTO);

	Boolean editLocation(LocationDTO locationDTO);

	Boolean editInventory(InventoryDTO inventoryDTO);

	Boolean editManifest(ManifestDTO manifestDTO) throws ParseException;
	
	

	Boolean createReasonCode(ReasonCodeDTO reasonCodeDTO);

	Boolean editReasonCode(ReasonCodeDTO reasonCodeDTO);
	
	Boolean createLocationFillingStatus(LocationFillingStatusDTO locationFillingStatusDTO);
	
	Boolean editLocationFillingStatus(LocationFillingStatusDTO locationFillingStatusDTO);

	Boolean createPickGroup(PickGroupDTO pickGroupDTO);

	Boolean editPickGroup(PickGroupDTO pickGroupDTO);


	Boolean deleteScanData(Long id);
	

	Boolean createTrain(TrainDTO trainDTO);

	Boolean editTrain(TrainDTO trainDTO);
	
	Boolean deleteTrain(Long TrainId);

	
	

	Boolean createTrolley(TrolleyDTO trolleyDTO);

	Boolean editTrolley(TrolleyDTO trolleyDTO);
	
	Boolean deleteTrolley(Long troleyID);

	

	Boolean createZoneDestination(ZoneDestinationDTO zoneDTO);

	Boolean editZoneDestination(ZoneDestinationDTO zoneDTO);
	
	Boolean deleteZoneDestination(Long zoneDestId);
	
	Boolean createOdetteLblHistory(OdetteLblHistoryDTO odetteLblHistoryDTO);


}
