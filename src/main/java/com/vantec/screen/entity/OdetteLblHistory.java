package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "odetteLblHistory")
public class OdetteLblHistory implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "ranOrder")
	private String ranOrder;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	@Column(name = "snp")
	private Integer snp;
	
	@Column(name = "noOfLabels")
	private Integer noOfLabels;
	
	@Column(name = "serialReference")
	private String serialReference;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public OdetteLblHistory() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRanOrder() {
		return ranOrder;
	}

	public void setRanOrder(String ranOrder) {
		this.ranOrder = ranOrder;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public Integer getSnp() {
		return snp;
	}

	public void setSnp(Integer snp) {
		this.snp = snp;
	}

	public Integer getNoOfLabels() {
		return noOfLabels;
	}

	public void setNoOfLabels(Integer noOfLabels) {
		this.noOfLabels = noOfLabels;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

    
	
	
	
	

}
