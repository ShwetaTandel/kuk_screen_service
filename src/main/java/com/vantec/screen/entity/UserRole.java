package com.vantec.screen.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(UserRole.class)
@Table(name = "userRole")
public class UserRole implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "userId")
	private Long userId;

	@Id
	@Column(name = "roleId")
	private Long roleId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	
	
	
	
	




}
