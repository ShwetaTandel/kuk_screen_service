package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "e2part")
public class E2Part implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "version")
	private Long version;

	@Column(name = "partNumber")
	private String partNumber;

	@Column(name = "locationDesc")
	private String locationDesc;

	@Column(name = "itemCost")
	private Double itemCost;

	@Column(name = "ddcPartFlag")
	private String ddcPartFlag;

	@Column(name = "ddcOffset")
	private String ddcOffset;

	@Column(name = "countFreq")
	private String countFreq;

	@Column(name = "weight")
	private Double weight;

	@Column(name = "commodity")
	private String commodity;

	@Column(name = "safetyTime")
	private Integer safetyTime;

	@Column(name = "purchaseText")
	private String purchaseText;

	@Column(name = "supplier")
	private String supplier;
	
	@Column(name = "dateCreated")
	private Date dateCreated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getLocationDesc() {
		return locationDesc;
	}

	public void setLocationDesc(String locationDesc) {
		this.locationDesc = locationDesc;
	}

	public Double getItemCost() {
		return itemCost;
	}

	public void setItemCost(Double itemCost) {
		this.itemCost = itemCost;
	}

	public String getDdcPartFlag() {
		return ddcPartFlag;
	}

	public void setDdcPartFlag(String ddcPartFlag) {
		this.ddcPartFlag = ddcPartFlag;
	}

	public String getDdcOffset() {
		return ddcOffset;
	}

	public void setDdcOffset(String ddcOffset) {
		this.ddcOffset = ddcOffset;
	}

	public String getCountFreq() {
		return countFreq;
	}

	public void setCountFreq(String countFreq) {
		this.countFreq = countFreq;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getCommodity() {
		return commodity;
	}

	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}

	public Integer getSafetyTime() {
		return safetyTime;
	}

	public void setSafetyTime(Integer safetyTime) {
		this.safetyTime = safetyTime;
	}

	public String getPurchaseText() {
		return purchaseText;
	}

	public void setPurchaseText(String purchaseText) {
		this.purchaseText = purchaseText;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

}
