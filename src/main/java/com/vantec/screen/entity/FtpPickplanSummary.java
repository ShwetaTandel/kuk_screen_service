package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ftpPickplanSummary")
public class FtpPickplanSummary implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "externalOrder")
	private String externalOrder;

	@Column(name = "pstatus")
	private String pstatus;

	@Column(name = "transType")
	private String transType;

	@Column(name = "operation")
	private String operation;

	@Column(name = "parentPart")
	private String parentPart;

	@Column(name = "productionDate")
	private Integer productionDate;
	
	@Column(name = "productionTime")
	private Integer productionTime;
	
	@Column(name = "buildDate")
	private Integer buildDate;
	
	@Column(name = "buildTime")
	private Integer buildTime;
	
	@Column(name = "salesDate")
	private Integer salesDate;
	
	@Column(name = "salesTime")
	private Integer salesTime;
	
	@Column(name = "distributor")
	private String distributor;

	@Column(name = "distributorName")
	private String distributorName;

	@Column(name = "project")
	private String project;

	@Column(name = "item")
	private String item;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "dateUpdated")
	private Date dateUpdated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExternalOrder() {
		return externalOrder;
	}

	public void setExternalOrder(String externalOrder) {
		this.externalOrder = externalOrder;
	}

	public String getPstatus() {
		return pstatus;
	}

	public void setPstatus(String pstatus) {
		this.pstatus = pstatus;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getParentPart() {
		return parentPart;
	}

	public void setParentPart(String parentPart) {
		this.parentPart = parentPart;
	}

	public Integer getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(Integer productionDate) {
		this.productionDate = productionDate;
	}

	public Integer getProductionTime() {
		return productionTime;
	}

	public void setProductionTime(Integer productionTime) {
		this.productionTime = productionTime;
	}

	public Integer getBuildDate() {
		return buildDate;
	}

	public void setBuildDate(Integer buildDate) {
		this.buildDate = buildDate;
	}

	public Integer getBuildTime() {
		return buildTime;
	}

	public void setBuildTime(Integer builTime) {
		this.buildTime = builTime;
	}

	public Integer getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(Integer salesDate) {
		this.salesDate = salesDate;
	}

	public Integer getSalesTime() {
		return salesTime;
	}

	public void setSalesTime(Integer salesTime) {
		this.salesTime = salesTime;
	}

	public String getDistributor() {
		return distributor;
	}

	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}

	public String getDistributorName() {
		return distributorName;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

}
