package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ftpPickplan")
public class FtpPickplan implements Serializable {

	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "PP1ORNO")
	private String PP1ORNO;

	@Column(name = "PP1STS")
	private String PP1STS;
	
	@Column(name = "PP1PSTMP")
	private String PP1PSTMP;

	@Column(name = "PP1PONO")
	private String PP1PONO;

	@Column(name = "PP1TYP")
	private String PP1TYP;
	
	@Column(name = "PP1TTYPE")
	private String PP1TTYPE;
	
	@Column(name = "PP1PARENT")
	private String PP1PARENT;
	
	@Column(name = "PP1PROJECT")
	private String PP1PROJECT;

	@Column(name = "PP1PNO")
	private String PP1PNO;

	@Column(name = "PP1PCQ")
	private Integer PP1PCQ;

	@Column(name = "PP1OPER")
	private String PP1OPER;
	
	@Column(name = "PP1ILOC")
	private String PP1ILOC;
	
	@Column(name = "PP1ITM")
	private String PP1ITM;
	
	@Column(name = "PP1BSTMP")
	private String PP1BSTMP;
	
	@Column(name = "PP1SSTMP")
	private String PP1SSTMP;
	
	@Column(name = "PP1DSTR")
	private String PP1DSTR;
	
	@Column(name = "PP1DNM")
	private String PP1DNM;
	
	@Column(name = "PP1PROJ")
	private String PP1PROJ;


	@Column(name = "dateCreated")
	private Date dateCreated;


	public String getPP1TTYPE() {
		return PP1TTYPE;
	}
	public void setPP1TTYPE(String pP1TYPE) {
		PP1TTYPE = pP1TYPE;
	}
	public String getPP1PARENT() {
		return PP1PARENT;
	}
	public void setPP1PARENT(String pP1PARENT) {
		PP1PARENT = pP1PARENT;
	}
	public String getPP1PROJECT() {
		return PP1PROJECT;
	}
	public void setPP1PROJECT(String pP1PROJECT) {
		PP1PROJECT = pP1PROJECT;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Long getId() {
		return id;
	}
	public String getPP1ORNO() {
		return PP1ORNO;
	}
	public String getPP1STS() {
		return PP1STS;
	}
	public String getPP1PSTMP() {
		return PP1PSTMP;
	}
	public String getPP1PONO() {
		return PP1PONO;
	}
	public String getPP1TYP() {
		return PP1TYP;
	}
	public String getPP1PNO() {
		return PP1PNO;
	}
	public Integer getPP1PCQ() {
		return PP1PCQ;
	}
	public String getPP1OPER() {
		return PP1OPER;
	}
	public String getPP1ILOC() {
		return PP1ILOC;
	}
	public String getPP1ITM() {
		return PP1ITM;
	}
	public String getPP1BSTMP() {
		return PP1BSTMP;
	}
	public String getPP1SSTMP() {
		return PP1SSTMP;
	}
	public String getPP1DSTR() {
		return PP1DSTR;
	}
	public String getPP1DNM() {
		return PP1DNM;
	}
	public String getPP1PROJ() {
		return PP1PROJ;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setPP1ORNO(String pP1ORNO) {
		PP1ORNO = pP1ORNO;
	}
	public void setPP1STS(String pP1STS) {
		PP1STS = pP1STS;
	}
	public void setPP1PSTMP(String pP1PSTMP) {
		PP1PSTMP = pP1PSTMP;
	}
	public void setPP1PONO(String pP1PONO) {
		PP1PONO = pP1PONO;
	}
	public void setPP1TYP(String pP1TYP) {
		PP1TYP = pP1TYP;
	}
	public void setPP1PNO(String pP1PNO) {
		PP1PNO = pP1PNO;
	}
	public void setPP1PCQ(Integer pP1PCQ) {
		PP1PCQ = pP1PCQ;
	}
	public void setPP1OPER(String pP1OPER) {
		PP1OPER = pP1OPER;
	}
	public void setPP1ILOC(String pP1ILOC) {
		PP1ILOC = pP1ILOC;
	}
	public void setPP1ITM(String pP1ITM) {
		PP1ITM = pP1ITM;
	}
	public void setPP1BSTMP(String pP1BSTMP) {
		PP1BSTMP = pP1BSTMP;
	}
	public void setPP1SSTMP(String pP1SSTMP) {
		PP1SSTMP = pP1SSTMP;
	}
	public void setPP1DSTR(String pP1DSTR) {
		PP1DSTR = pP1DSTR;
	}
	public void setPP1DNM(String pP1DNM) {
		PP1DNM = pP1DNM;
	}
	public void setPP1PROJ(String pP1PROJ) {
		PP1PROJ = pP1PROJ;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	
}
