package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "userLevel")
public class UserLevel implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "userLevel")
	private String userLevel;
	
	@Column(name = "rdtReceiving")
	private Boolean rdtReceiving;
	
	@Column(name = "rdtPicking")
	private Boolean rdtPicking;
	
	@Column(name = "rdtPi")
	private Boolean rdtPi;
	
	@Column(name = "rdtReplen")
	private Boolean rdtReplen;
	
	@Column(name = "rdtQa")
	private Boolean rdtQa;
	
	@Column(name = "rdtOther")
	private Boolean rdtOther;
	
	@Column(name = "fsStaticdata")
	private Boolean fsStaticdata;
	
	@Column(name = "fsProcessing")
	private Boolean fsProcessing;
	
	@Column(name = "fsEnquires")
	private Boolean fsEnquires;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(String userLevel) {
		this.userLevel = userLevel;
	}

	public Boolean getRdtReceiving() {
		return rdtReceiving;
	}

	public void setRdtReceiving(Boolean rdtReceiving) {
		this.rdtReceiving = rdtReceiving;
	}

	public Boolean getRdtPicking() {
		return rdtPicking;
	}

	public void setRdtPicking(Boolean rdtPicking) {
		this.rdtPicking = rdtPicking;
	}

	public Boolean getRdtPi() {
		return rdtPi;
	}

	public void setRdtPi(Boolean rdtPi) {
		this.rdtPi = rdtPi;
	}

	public Boolean getRdtReplen() {
		return rdtReplen;
	}

	public void setRdtReplen(Boolean rdtReplen) {
		this.rdtReplen = rdtReplen;
	}

	public Boolean getRdtQa() {
		return rdtQa;
	}

	public void setRdtQa(Boolean rdtQa) {
		this.rdtQa = rdtQa;
	}

	public Boolean getRdtOther() {
		return rdtOther;
	}

	public void setRdtOther(Boolean rdtOther) {
		this.rdtOther = rdtOther;
	}

	public Boolean getFsStaticdata() {
		return fsStaticdata;
	}

	public void setFsStaticdata(Boolean fsStaticdata) {
		this.fsStaticdata = fsStaticdata;
	}

	public Boolean getFsProcessing() {
		return fsProcessing;
	}

	public void setFsProcessing(Boolean fsProcessing) {
		this.fsProcessing = fsProcessing;
	}

	public Boolean getFsEnquires() {
		return fsEnquires;
	}

	public void setFsEnquires(Boolean fsEnquires) {
		this.fsEnquires = fsEnquires;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
	




}
