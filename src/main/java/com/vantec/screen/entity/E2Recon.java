package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "e2reconc")
public class E2Recon implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "partNumber")
	private String partNumber;
	
	@Column(name = "partId")
	private Long partId;

	@Column(name = "kukQty")
	private Double kukQty;
	
	@Column(name = "vimsQty")
	private Double vimsQty;
	
	@Column(name = "vimsHoldQty")
	private Double vimsHoldQty;
	
	@Column(name = "diffQty")
	private Double diffQty;
	
	@Column(name = "backFlush")
	private Boolean backFlush;
	
	@Column(name = "dateCreated")
	private Date dateCreated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getVimsHoldQty() {
		return vimsHoldQty;
	}

	public void setVimsHoldQty(Double vimsHoldQty) {
		this.vimsHoldQty = vimsHoldQty;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public Long getPartId() {
		return partId;
	}

	public void setPartId(Long partId) {
		this.partId = partId;
	}

	public Double getKukQty() {
		return kukQty;
	}

	public void setKukQty(Double kukQty) {
		this.kukQty = kukQty;
	}

	public Double getVimsQty() {
		return vimsQty;
	}

	public void setVimsQty(Double vimsQty) {
		this.vimsQty = vimsQty;
	}

	public Double getDiffQty() {
		return diffQty;
	}

	public void setDiffQty(Double diffQty) {
		this.diffQty = diffQty;
	}

	public Boolean getBackFlush() {
		return backFlush;
	}

	public void setBackFlush(Boolean backFlush) {
		this.backFlush = backFlush;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}


}
