package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jobLog")
public class JobLog implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "jobName")
	private String jobName;

	@Column(name = "fileProcessed")
	private String fileProcessed;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "dateCreated")
	private Date dateCreated;

	@Column(name = "jobStarted")
	private Date jobStarted;
	
	@Column(name = "jobEnded")
	private Date jobEnded;

	@Column(name = "createdBy")
	private String createdBy;
	
	public JobLog() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getFileProcessed() {
		return fileProcessed;
	}

	public void setFileProcessed(String fileProcessed) {
		this.fileProcessed = fileProcessed;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getJobStarted() {
		return jobStarted;
	}

	public void setJobStarted(Date jobStarted) {
		this.jobStarted = jobStarted;
	}

	public Date getJobEnded() {
		return jobEnded;
	}

	public void setJobEnded(Date jobEnded) {
		this.jobEnded = jobEnded;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
}
