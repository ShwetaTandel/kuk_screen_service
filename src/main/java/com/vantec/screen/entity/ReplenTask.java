package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "replenTask")
public class ReplenTask implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "fromLocationCode")
	private String fromLocationCode;
	
	@Column(name = "pickGroupId")
	private Long pickGroupId;
	
	@Column(name = "inventoryMasterId")
	private Long inventoryMasterId;
	
	@Column(name = "partId")
	private Long partId;
	
	@Column(name = "toLocationCode")
	private String toLocationCode;
	
	@Column(name = "priority")
	private Integer priority;
	
	@Column(name = "inUse")
	private String inUse;
	
	@Column(name = "processed")
	private Boolean processed = false;
	
	@Column(name = "suggestedSerial")
	private String suggestedSerial;
	
	@Column(name = "scannedSerial")
	private String scannedSerial;
	
	@Column(name = "palletReference")
	private String palletReference;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public ReplenTask() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPickGroupId() {
		return pickGroupId;
	}

	public void setPickGroupId(Long pickGroupId) {
		this.pickGroupId = pickGroupId;
	}

	public Long getInventoryMasterId() {
		return inventoryMasterId;
	}

	public void setInventoryMasterId(Long inventoryMasterId) {
		this.inventoryMasterId = inventoryMasterId;
	}

	public Long getPartId() {
		return partId;
	}

	public void setPartId(Long partId) {
		this.partId = partId;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public String getInUse() {
		return inUse;
	}

	public void setInUse(String inUse) {
		this.inUse = inUse;
	}

	public Boolean getProcessed() {
		return processed;
	}

	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getFromLocationCode() {
		return fromLocationCode;
	}

	public void setFromLocationCode(String fromLocationCode) {
		this.fromLocationCode = fromLocationCode;
	}

	public String getToLocationCode() {
		return toLocationCode;
	}

	public void setToLocationCode(String toLocationCode) {
		this.toLocationCode = toLocationCode;
	}

	public String getSuggestedSerial() {
		return suggestedSerial;
	}

	public void setSuggestedSerial(String suggestedSerial) {
		this.suggestedSerial = suggestedSerial;
	}

	public String getScannedSerial() {
		return scannedSerial;
	}

	public void setScannedSerial(String scannedSerial) {
		this.scannedSerial = scannedSerial;
	}

	public String getPalletReference() {
		return palletReference;
	}

	public void setPalletReference(String palletReference) {
		this.palletReference = palletReference;
	}
	
	
}
