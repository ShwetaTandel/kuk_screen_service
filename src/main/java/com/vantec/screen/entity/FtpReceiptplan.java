package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ftpReceiptplan")
public class FtpReceiptplan implements Serializable {

	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;


	@Column(name = "PR1ORNO")
	private String PR1ORNO;

	@Column(name = "PR1PONO")
	private String PR1PONO;
	@Column(name = "PR1PNO")
	private String PR1PNO;

	@Column(name = "PR1PCQ")
	private Integer PR1PCQ;

	@Column(name = "PR1RDT")
	private Integer PR1RDT;

	@Column(name = "PR1PCD")
	private String PR1PCD;

	@Column(name = "PR1PNM")
	private String PR1PNM;

	@Column(name = "PR1PFL")
	private String PR1PFL;

	@Column(name = "dateCreated")
	private Date dateCreated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPR1ORNO() {
		return PR1ORNO;
	}

	public void setPR1ORNO(String pR1ORNO) {
		PR1ORNO = pR1ORNO;
	}

	public String getPR1PONO() {
		return PR1PONO;
	}

	public void setPR1PONO(String pR1PONO) {
		PR1PONO = pR1PONO;
	}

	public String getPR1PNO() {
		return PR1PNO;
	}

	public void setPR1PNO(String pR1PNO) {
		PR1PNO = pR1PNO;
	}

	public Integer getPR1PCQ() {
		return PR1PCQ;
	}

	public void setPR1PCQ(Integer pR1PCQ) {
		PR1PCQ = pR1PCQ;
	}

	public Integer getPR1RDT() {
		return PR1RDT;
	}

	public void setPR1RDT(Integer pR1RDT) {
		PR1RDT = pR1RDT;
	}

	public String getPR1PCD() {
		return PR1PCD;
	}

	public void setPR1PCD(String pR1PCD) {
		PR1PCD = pR1PCD;
	}

	public String getPR1PNM() {
		return PR1PNM;
	}

	public void setPR1PNM(String pR1PNM) {
		PR1PNM = pR1PNM;
	}

	public String getPR1PFL() {
		return PR1PFL;
	}

	public void setPR1PFL(String pR1PFL) {
		PR1PFL = pR1PFL;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
}
