package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "replenTransferHistory")
public class ReplenTransferHistory implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "locationCode")
	private String locationCode;
	
	@Column(name = "newLocationCode")
	private String newLocationCode;
	
	@Column(name = "fromPickGroupId")
	private Long fromPickGroupId;
	
	@Column(name = "toPickGroupId")
	private Long toPickGroupId;
	
	@Column(name = "fromPriority")
	private Integer fromPriority;
	
	@Column(name = "toPriority")
	private Integer toPriority;
	
	@Column(name = "inventoryMasterId")
	private Long inventoryMasterId;
	
	@Column(name = "partId")
	private Long partId;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	public ReplenTransferHistory() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFromPickGroupId() {
		return fromPickGroupId;
	}

	public void setFromPickGroupId(Long fromPickGroupId) {
		this.fromPickGroupId = fromPickGroupId;
	}

	public Long getToPickGroupId() {
		return toPickGroupId;
	}

	public void setToPickGroupId(Long toPickGroupId) {
		this.toPickGroupId = toPickGroupId;
	}

	public Long getInventoryMasterId() {
		return inventoryMasterId;
	}

	public void setInventoryMasterId(Long inventoryMasterId) {
		this.inventoryMasterId = inventoryMasterId;
	}

	public Long getPartId() {
		return partId;
	}

	public void setPartId(Long partId) {
		this.partId = partId;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getNewLocationCode() {
		return newLocationCode;
	}

	public void setNewLocationCode(String newLocationCode) {
		this.newLocationCode = newLocationCode;
	}

	public Integer getFromPriority() {
		return fromPriority;
	}

	public void setFromPriority(Integer fromPriority) {
		this.fromPriority = fromPriority;
	}

	public Integer getToPriority() {
		return toPriority;
	}

	public void setToPriority(Integer toPriority) {
		this.toPriority = toPriority;
	}
	
	


	
}
