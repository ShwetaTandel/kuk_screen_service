package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "part")
public class Part implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;

	@Column(name = "partNumber")
	private String partNumber;

	@Column(name = "effectiveFrom")
	private Date effectiveFrom;

	@Column(name = "effectiveTo")
	private Date effectiveTo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "packTypeId", referencedColumnName = "id")
	private PackType packType;

	@Column(name = "partDescription")
	private String partDescription;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vendorId", referencedColumnName = "id")
	private Vendor vendor;

	@Column(name = "jisSupplyGroup")
	private String jisSupplyGroup;

	@Column(name = "wiCode")
	private String wiCode;

//	@Column(name = "fixedLocationCode")
//	private String fixedLocationCode;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fixedLocationTypeId", referencedColumnName = "id")
	private LocationType fixedLocationType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fixedSubTypeId", referencedColumnName = "id")
	private LocationSubType fixedSubType;
	
	@Column(name = "imaginaryPart")
	private Boolean imaginaryPart = false;

	@Column(name = "requiresCount")
	private Boolean requiresCount = false;
	
	@Column(name = "updateSerialWithCount")
	private Boolean updateSerialWithCount = false;

	//decant
	@Column(name = "requiresDecant")
	private Boolean requiresDecant = false;

	//inspect
	@Column(name = "requiresInspection")
	private Boolean requiresInspection = false;
	
	@Column(name = "safetyStock")
	private Integer safetyStock;

	@Column(name = "miniStock")
	private Integer miniStock;

	@Column(name = "shelfLife")
	private Integer shelfLife;
	
	@Column(name = "includeInventory")
	private Integer includeInventory;
	
	@Column(name = "conversionFactor")
	private Integer conversionFactor;

	@Column(name = "importantA")
	private Boolean importantA = false;
	
	@Column(name = "highValue")
	private Boolean highValue = false;
	
	@Column(name = "autoReceive")
	private Boolean autoReceive = false;
	
	@Column(name = "autoPick")
	private Boolean autoPick = false;
	
	@Column(name = "fullBoxPick")
	private Boolean fullBoxPick = false;
	
	@Column(name = "active")
	private Boolean active = true;
	
	@Column(name = "modular")
	private Boolean modular = false;
	
	
	@Column(name = "isBigPart")
	private Boolean isBigPart = false;
	
	@Column(name = "isAS400")
	private Boolean isAS400 =false;
	//rdtMessage
	@Column(name = "rdtPrompt")
	private String rdtPrompt;

	@Column(name = "partFamily")
	private String partFamily;
	
	@Column(name = "piecePartNumber")
	private String piecePartNumber;

	//loosePcsEqualTo1
	@Column(name = "loosePartQty")
	private Integer loosePartQty = 0;
	
	@Column(name = "weight")
	private Double weight;
	
	@Column(name = "stackHeight")
	private Integer stackHeight;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date lastUpdated;

	@Column(name = "lastUpdatedBy")
	private String lastUpdatedBy;

	public Part() {
	}

    

	public Boolean getIsAS400() {
		return isAS400;
	}



	public void setIsAS400(Boolean isAS400) {
		this.isAS400 = isAS400;
	}



	public Boolean getIsBigPart() {
		return isBigPart;
	}



	public void setIsBigPart(Boolean isBigPart) {
		this.isBigPart = isBigPart;
	}



	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return effectiveTo;

	
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}
	public String getPartDescription() {
		return partDescription;
	}

	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}
	

	public String getJisSupplyGroup() {
		return jisSupplyGroup;
	}



	public void setJisSupplyGroup(String jisSupplyGroup) {
		this.jisSupplyGroup = jisSupplyGroup;
	}



	public String getWiCode() {
		return wiCode;
	}

	public void setWiCode(String wiCode) {
		this.wiCode = wiCode;
	}

	public Boolean getRequiresCount() {
		return requiresCount;
	}



	public void setRequiresCount(Boolean requiresCount) {
		this.requiresCount = requiresCount;
	}



	public Boolean getActive() {
		return active;
	}



	public void setActive(Boolean active) {
		this.active = active;
	}



	public Boolean getUpdateSerialWithCount() {
		return updateSerialWithCount;
	}

	public void setUpdateSerialWithCount(Boolean updateSerialWithCount) {
		this.updateSerialWithCount = updateSerialWithCount;
	}
	public Integer getMiniStock() {
		return miniStock;
	}

	public void setMiniStock(Integer miniStock) {
		this.miniStock = miniStock;
	}

	public Integer getShelfLife() {
		return shelfLife;
	}

	public void setShelfLife(Integer shelfLife) {
		this.shelfLife = shelfLife;
	}

	public Boolean getImportantA() {
		return importantA;
	}

	public void setImportantA(Boolean importantA) {
		this.importantA = importantA;
	}

	public Boolean getHighValue() {
		return highValue;
	}

	public void setHighValue(Boolean highValue) {
		this.highValue = highValue;
	}

	public Boolean getAutoReceive() {
		return autoReceive;
	}

	public void setAutoReceive(Boolean autoReceive) {
		this.autoReceive = autoReceive;
	}

	public Boolean getAutoPick() {
		return autoPick;
	}

	public void setAutoPick(Boolean autoPick) {
		this.autoPick = autoPick;
	}

	public String getPartFamily() {
		return partFamily;
	}

	public void setPartFamily(String partFamily) {
		this.partFamily = partFamily;
	}
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}



	public Date getLastUpdated() {
		return lastUpdated;
	}



	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}



	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}



	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}




	public PackType getPackType() {
		return packType;
	}
	public void setPackType(PackType packType) {
		this.packType = packType;
	}
	public LocationType getFixedLocationType() {
		return fixedLocationType;
	}
	public void setFixedLocationType(LocationType fixedLocationType) {
		this.fixedLocationType = fixedLocationType;
	}

	public LocationSubType getFixedSubType() {
		return fixedSubType;
	}
	public void setFixedSubType(LocationSubType fixedSubType) {
		this.fixedSubType = fixedSubType;
	}
	public Boolean getRequiresDecant() {
		return requiresDecant;
	}
	public void setRequiresDecant(Boolean requiresDecant) {
		this.requiresDecant = requiresDecant;
	}
	public Boolean getRequiresInspection() {
		return requiresInspection;
	}
	public void setRequiresInspection(Boolean requiresInspection) {
		this.requiresInspection = requiresInspection;
	}
	public String getRdtPrompt() {
		return rdtPrompt;
	}
	public void setRdtPrompt(String rdtPrompt) {
		this.rdtPrompt = rdtPrompt;
	}
	public Integer getLoosePartQty() {
		return loosePartQty;
	}
	public void setLoosePartQty(Integer loosePartQty) {
		this.loosePartQty = loosePartQty;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public Boolean getImaginaryPart() {
		return imaginaryPart;
	}

	public void setImaginaryPart(Boolean imaginaryPart) {
		this.imaginaryPart = imaginaryPart;
	}
	public Integer getSafetyStock() {
		return safetyStock;
	}
	public void setSafetyStock(Integer safetyStock) {
		this.safetyStock = safetyStock;
	}public Vendor getVendor() {
		return vendor;
	}
	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
	public Boolean getFullBoxPick() {
		return fullBoxPick;
	}
	public void setFullBoxPick(Boolean fullBoxPick) {
		this.fullBoxPick = fullBoxPick;
	}
	public Boolean getModular() {
		return modular;
	}
	public void setModular(Boolean modular) {
		this.modular = modular;
	}
	public Integer getIncludeInventory() {
		return includeInventory;
	}
	public void setIncludeInventory(Integer includeInventory) {
		this.includeInventory = includeInventory;
	}
	public Integer getConversionFactor() {
		return conversionFactor;
	}
	public void setConversionFactor(Integer conversionFactor) {
		this.conversionFactor = conversionFactor;
	}
	public String getPiecePartNumber() {
		return piecePartNumber;
	}
	public void setPiecePartNumber(String piecePartNumber) {
		this.piecePartNumber = piecePartNumber;
	}



	public Integer getStackHeight() {
		return stackHeight;
	}



	public void setStackHeight(Integer stackHeight) {
		this.stackHeight = stackHeight;
	}

}
