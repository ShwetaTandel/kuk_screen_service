package com.vantec.screen.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "AllocationDetail")
@JsonIgnoreProperties(ignoreUnknown=true)
public class AllocationDetail implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "receiptBodyId", referencedColumnName = "id")
	private ReceiptBody receiptBody;

	@Column(name = "serialReference")
	private String serialReference;
	
	@Column(name = "advisedQty")
	private Double advisedQty;

	@Column(name = "receivedQty")
	private Double receivedQty;
	
	@Column(name = "allocatedQty")
	private Double allocatedQty;
	
	@Column(name = "processed")
	private Boolean processed;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventoryMasterId", referencedColumnName = "id")
	private InventoryMaster inventory;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public AllocationDetail() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ReceiptBody getReceiptBody() {
		return receiptBody;
	}

	public void setReceiptBody(ReceiptBody receiptBody) {
		this.receiptBody = receiptBody;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public Double getAdvisedQty() {
		return advisedQty;
	}

	public void setAdvisedQty(Double advisedQty) {
		this.advisedQty = advisedQty;
	}

	public Double getReceivedQty() {
		return receivedQty;
	}

	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Boolean getProcessed() {
		return processed;
	}

	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}

	public InventoryMaster getInventory() {
		return inventory;
	}

	public void setInventory(InventoryMaster inventory) {
		this.inventory = inventory;
	}

	public Double getAllocatedQty() {
		return allocatedQty;
	}

	public void setAllocatedQty(Double allocatedQty) {
		this.allocatedQty = allocatedQty;
	}
	
	

}
