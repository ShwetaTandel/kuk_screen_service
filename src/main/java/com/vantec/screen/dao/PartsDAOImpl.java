package com.vantec.screen.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Repository
public class PartsDAOImpl implements PartsDAO {


	@PersistenceContext
	private EntityManager entityManager;
	@Override
	public boolean updatePartsWithDescription(){
		
		StoredProcedureQuery  updateQuery = entityManager.createStoredProcedureQuery("update_part_from_e2part");
		boolean result = updateQuery.execute();
		entityManager.clear();
		return result;
	}
	

}
