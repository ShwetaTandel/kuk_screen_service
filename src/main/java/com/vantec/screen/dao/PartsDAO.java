package com.vantec.screen.dao;

import org.springframework.stereotype.Component;


@Component
public interface PartsDAO {
	
	public boolean updatePartsWithDescription();

}
