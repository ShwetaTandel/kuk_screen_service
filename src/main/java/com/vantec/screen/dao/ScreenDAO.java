package com.vantec.screen.dao;

import com.vantec.screen.entity.LocationAisle;
import com.vantec.screen.entity.LocationColumn;
import com.vantec.screen.entity.LocationRack;
import com.vantec.screen.entity.LocationRow;
import com.vantec.screen.entity.LocationSubType;
import com.vantec.screen.entity.LocationType;
import com.vantec.screen.entity.Part;
import com.vantec.screen.entity.Vendor;


public interface ScreenDAO {

	boolean addVendor(Vendor vendor);
	

	boolean createZoneType(LocationSubType zoneType);

	boolean createRow(LocationRow row);

	boolean createAisle(LocationAisle aisle);

	boolean createColumn(LocationColumn column);

	boolean createLocationType(LocationType locationType);

	boolean createPart(Part part);


	boolean createRack(LocationRack rack);
	

}
