package com.vantec.screen.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.screen.entity.LocationAisle;
import com.vantec.screen.entity.LocationColumn;
import com.vantec.screen.entity.LocationRack;
import com.vantec.screen.entity.LocationRow;
import com.vantec.screen.entity.LocationSubType;
import com.vantec.screen.entity.LocationType;
import com.vantec.screen.entity.Part;
import com.vantec.screen.entity.Vendor;

@Transactional
@Repository
public class ScreenDAOImpl implements ScreenDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public boolean addVendor(Vendor vendor) {
		entityManager.persist(vendor);
		return true;
	}

	
	@Override
	public boolean createZoneType(LocationSubType zoneType) {
		entityManager.persist(zoneType);
		return true;
	}

	@Override
	public boolean createRow(LocationRow row) {
		entityManager.persist(row);
		return true;
	}

	@Override
	public boolean createAisle(LocationAisle aisle) {
		entityManager.persist(aisle);
		return true;
	}

	@Override
	public boolean createColumn(LocationColumn column) {
		entityManager.persist(column);
		return true;
	}

	@Override
	public boolean createLocationType(LocationType locationType) {
		entityManager.persist(locationType);
		return true;
	}

	@Override
	public boolean createPart(Part part) {
		entityManager.persist(part);
		return true;
	}

	

	@Override
	public boolean createRack(LocationRack rack) {
		entityManager.persist(rack);
		return true;
	}
	
	
	
	
	

}
